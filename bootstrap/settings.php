<?php
return [
  'settings' => [
    'displayErrorDetails' => true,
    'db' => [
      'driver'    => 'mysql',
      'host'      => 'localhost',
      'database'  => 'webpranzo',
      'username'  => 'webpranzo_admin',
      'password'  => '',
      'charset'   => 'utf8',
      'collation' => 'utf8_general_ci',
      'prefix'    => '',
    ],
  ],
];