<?php

use WebPranzo\Middleware\AuthMiddleware;
use WebPranzo\Middleware\GuestMiddleware;
use WebPranzo\Middleware\ClienteMiddleware;
use WebPranzo\Middleware\FornitoreMiddleware;
use WebPranzo\Middleware\AdminMiddleware;
use WebPranzo\Middleware\FattorinoMiddleware;

$app->get('/', 'HomeController:index')->setName('home');

$app->group('', function() {
  // Questi route sono destinati solo per i visitatori non loggati
  $this->get('/auth/registra', 'AuthController:getPreRegistra')->setName('auth.registra');
  $this->post('/auth/registra', 'AuthController:postPreRegistra');
  $this->get('/auth/registra/cliente', 'AuthController:getRegistraCliente')->setName('auth.registraCliente');
  $this->post('/auth/registra/cliente', 'AuthController:postRegistraCliente');
  $this->get('/auth/registra/fornitore', 'AuthController:getRegistraFornitore')->setName('auth.registraFornitore');
  $this->post('/auth/registra/fornitore', 'AuthController:postRegistraFornitore');
  $this->get('/auth/registra/amministratore', 'AuthController:getRegistraAmministratore')->setName('auth.registraAmministratore');
  $this->post('/auth/registra/amministratore', 'AuthController:postRegistraAmministratore');

  $this->get('/auth/login', 'AuthController:getLogin')->setName('auth.login');
  $this->post('/auth/login', 'AuthController:postLogin');
})->add(new GuestMiddleware($container));

$app->group('', function() {
  // Questi route sono destinati per qualsiasi utente loggato
  $this->get('/profilo', 'ProfiloController:getProfilo')->setName('utente.profilo');
  $this->post('/profilo', 'ProfiloController:postProfilo');
  
  $this->get('/orario', 'ProfiloController:getOrario')->setName('utente.orario');
  $this->post('/orario', 'ProfiloController:postOrario');

  $this->get('/auth/logout', 'AuthController:Logout')->setName('auth.logout');

})->add(new AuthMiddleware($container));

$app->group('', function() {
  $this->get('/cliente/dashboard', 'DashboardClienteController:getDashboard')->setName('cliente.dashboard');
  $this->get('/cliente/ordini', 'OrdiniController:getOrdiniCliente')->setName('cliente.ordini');
  $this->get('/cliente/ordini/dettagli/{id}', 'OrdiniController:getDettagliOrdineCliente')->setName('cliente.ordini.dettagli');

  $this->get('/cliente/messaggi', 'MessaggiController:getMessaggiCliente')->setName('cliente.messaggi');
  $this->get('/cliente/feedbackOrdine/{id}', 'MessaggiController:getFeedbackCliente')->setName('cliente.feedback');
  $this->post('/cliente/feedbackOrdine/{id}', 'MessaggiController:postFeedbackCliente');

  $this->get('/carrello', 'CarrelloController:getCarrello')->setName('cliente.carrello');
  $this->post('/carrello', 'CarrelloController:postCarrello');
  $this->post('/carrello/procedi', 'CarrelloController:postCarrelloProcedi')->setName('cliente.carrello.procedi');
  $this->get('/carrello/pagamento', 'CarrelloController:getCarrelloPagamento')->setName('cliente.carrello.pagamento');
  $this->post('/carrello/pagamento', 'CarrelloController:postCarrelloPagamento');
  $this->get('/carrello/riepilogo', 'CarrelloController:getCarrelloRiepilogo')->setName('cliente.carrello.riepilogo');
  $this->post('/carrello/ordina', 'CarrelloController:postCarrelloOrdina')->setName('cliente.carrello.ordina');
})->add(new ClienteMiddleware($container));

$app->get('/ricerca', 'RicercaRistoranteController:getRicerca')->setName('cliente.ricerca');
$app->post('/ricerca', 'RicercaRistoranteController:postRicerca');

$app->get('/ricerca/risultati', 'RicercaRistoranteController:getRisultatoRicerca')->setName('cliente.ricerca.risultato');
$app->get('/ricerca/risultati/filtri', 'RicercaRistoranteController:getFiltriRicerca')->setName('cliente.ricerca.filtri');
$app->post('/ricerca/risultati/filtri', 'RicercaRistoranteController:postFiltriRicerca');

$app->group('', function() {
  $this->get('/fornitore/dashboard', 'DashboardFornitoreController:getDashboard')->setName('fornitore.dashboard');
  $this->get('/fornitore/ordini', 'OrdiniController:getOrdiniFornitore')->setName('fornitore.ordini');
  $this->post('/fornitore/ordini/evadere/{id}', 'OrdiniController:postOrdiniEvadere');
  $this->get('/fornitore/feedbackOrdini', 'MessaggiController:getFeedbackFornitore')->setName('fornitore.feedback');
  
  $this->get('/fornitore/listini', 'ListiniController:getListini')->setName('fornitore.listini');
  $this->get('/fornitore/listini/{id}', 'ListiniController:getListino')->setName('fornitore.listino');
  $this->post('/fornitore/listini/{id}', 'ListiniController:postListino');
  
  $this->get('/fornitore/listini/{idListino}/piatto/{idPiatto}', 'ListiniController:getPiatto')->setName('fornitore.listino.piatto');
  $this->post('/fornitore/listini/{idListino}/piatto/{idPiatto}', 'ListiniController:postPiatto');
  $this->get('/fornitore/listini/{idListino}/piatto/{idPiatto}/ingrediente/{idIngrediente}', 'ListiniController:getIngrediente')->setName('fornitore.listino.piatto.ingrediente');
  $this->post('/fornitore/listini/{idListino}/piatto/{idPiatto}/ingrediente/{idIngrediente}', 'ListiniController:postIngrediente');
})->add(new FornitoreMiddleware($container));

$app->get('/fornitore/{id}', 'InfoFornitoreController:getFornitore');

$app->get('/fornitore/{id}/menu', 'MenuFornitoreController:getMenu');

$app->get('/fornitore/{id}/feedback', 'MessaggiController:getFeedbackDelFornitore');

$app->get('/utente/{id}/indirizzo', 'ProfiloController:getIndirizzoUtente');

$app->group('', function() {
  $this->get('/admin/dashboard', 'DashboardAdminController:getDashboard')->setName('admin.dashboard');
  $this->get('/admin/utenti', 'ProfiloController:getElencoUtenti')->setName('admin.elencoUtenti');

  $this->get('/admin/utente/{id}', 'ProfiloController:getDettagliUtente')->setName('admin.dettagliUtente');
  $this->post('/admin/utente/{id}', 'ProfiloController:postDettagliUtente');
  
})->add(new AdminMiddleware($container));

$app->group('', function() {
  $this->get('/fattorino/ordini', 'OrdiniController:getOrdiniFattorino')->setName('fattorino.ordini');
  $this->post('/fattorino/ordine/{idOrdine}/changeStato/{idStato}', 'OrdiniController:postChangeStatoSpedizioneOrdine');
})->add(new FattorinoMiddleware($container));
