<?php
// Application middleware
// e.g: $app->add(new \Slim\Csrf\Guard);

$app->add(new \WebPranzo\Middleware\ValidationErrorsMiddleware($container));
$app->add(new \WebPranzo\Middleware\OldInputMiddleware($container));