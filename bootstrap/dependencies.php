<?php

$container = $app->getContainer();

// Setup a connection to MySQL
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function ($container) use ($capsule) {
  return $capsule;
};

$container['auth'] = function ($container) {
  return new \WebPranzo\Auth\Auth;
};

$container['flash'] = function ($container) {
  return new \Slim\Flash\Messages();
};

// view renderer
$container['view'] = function ($container) {
  $view = new \Slim\Views\Twig(__DIR__ . '/../resources/views', [
    'cache' => false
  ]);

  $view->addExtension(new \Slim\Views\TwigExtension(
    $container->router,
    $container->request->getUri()
  ));

  $view->getEnvironment()->addGlobal('auth', [
    'check' => $container->auth->check(),
    'user' => $container->auth->user(),
    'getUserCredentials' => $container->auth->getUserCredentials(),
  ]);

  $view->getEnvironment()->addGlobal('flash', $container->flash);
  $view->getEnvironment()->addGlobal('user', $container->auth->user());
  $view->getEnvironment()->addGlobal('current_path', $container["request"]->getUri()->getPath());

  return $view;
};

$container['validator'] = function ($container) {
  return new \WebPranzo\Validation\Validator;
};

// Controllers
$container['HomeController'] = function ($container) {
  return new \WebPranzo\Controllers\HomeController($container);
};
$container['AuthController'] = function ($container) {
  return new \WebPranzo\Controllers\Auth\AuthController($container);
};
$container['ProfiloController'] = function ($container) {
  return new \WebPranzo\Controllers\Utente\ProfiloController($container);
};
$container['PasswordController'] = function ($container) {
  return new \WebPranzo\Controllers\Auth\PasswordController($container);
};
$container['RicercaRistoranteController'] = function ($container) {
  return new \WebPranzo\Controllers\Cliente\RicercaRistoranteController($container);
};
$container['InfoFornitoreController'] = function ($container) {
  return new \WebPranzo\Controllers\Cliente\InfoFornitoreController($container);
};
$container['MenuFornitoreController'] = function ($container) {
  return new \WebPranzo\Controllers\Cliente\MenuFornitoreController($container);
};
$container['CarrelloController'] = function ($container) {
  return new \WebPranzo\Controllers\Cliente\CarrelloController($container);
};
$container['DashboardFornitoreController'] = function ($container) {
  return new \WebPranzo\Controllers\Fornitore\DashboardFornitoreController($container);
};
$container['DashboardClienteController'] = function ($container) {
  return new \WebPranzo\Controllers\Cliente\DashboardClienteController($container);
};
$container['OrdiniController'] = function ($container) {
  return new \WebPranzo\Controllers\Utente\OrdiniController($container);
};
$container['ListiniController'] = function ($container) {
  return new \WebPranzo\Controllers\Fornitore\ListiniController($container);
};
$container['MessaggiController'] = function ($container) {
  return new \WebPranzo\Controllers\Utente\MessaggiController($container);
};
$container['DashboardAdminController'] = function ($container) {
  return new \WebPranzo\Controllers\Admin\DashboardAdminController($container);
};