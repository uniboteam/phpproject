<?php

// Docs for API
// https://respect-validation.readthedocs.io/en/1.1/
namespace WebPranzo\Validation;
use Respect\Validation\Validator as Respect;
use Respect\Validation\Exceptions\NestedValidationException;

class Validator
{
  protected $errors;
  public function validate($req, array $rules)
  {
    foreach ($rules as $field => $rule) {
      try {
        $rule->setName(ucFirst($field))->assert($req->getParam($field));
      } catch (NestedValidationException $e) {
        $e->findMessages([
          'alpha'        => 'Il campo deve essere alfanumerico',
          'numeric'      => 'Il campo deve essere numerico',
          'email'        => 'Email deve essere corretta',
          'notEmpty'     => 'Il campo non deve essere vuoto',
          'noWhitespace' => 'Il campo non deve avere gli spazi',
          'equals'       => 'Le password devono essere uguali',
        ]);
        $this->errors[$field] = $e->getMessages();
      }
    }

    $_SESSION['errors'] = $this->errors;

    return $this;
  }

  public function failed()
  {
    return !empty($this->errors);
  }
}