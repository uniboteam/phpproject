<?php

namespace WebPranzo\Validation\Exceptions;
use Respect\Validation\Exceptions\ValidationException;

class MatchesPasswordException extends ValidationException
{
  public static $defaultTemplates = [
    self::MODE_DEFAULT => [
      self::STANDARD => 'La password non corrisponde'
    ]
  ];
}