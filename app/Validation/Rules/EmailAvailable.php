<?php

namespace WebPranzo\Validation\Rules;
use Respect\Validation\Rules\AbstractRule;
use WebPranzo\Models\Utente;

class EmailAvailable extends AbstractRule
{
  public function validate($input)
  {
    return Utente::where('email', $input)->count() === 0;
  }
}