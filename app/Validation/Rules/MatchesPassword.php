<?php

namespace WebPranzo\Validation\Rules;
use Respect\Validation\Rules\AbstractRule;
use WebPranzo\Models\Utente;

class MatchesPassword extends AbstractRule
{
  protected $password;
  public function __construct($password)
  {
    $this->password = $password;
  }

  public function validate($input)
  {
    return \password_verify($input, $this->password);
  }
}