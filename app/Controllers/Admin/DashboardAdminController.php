<?php

namespace WebPranzo\Controllers\Admin;
use WebPranzo\Controllers\Controller;
use WebPranzo\Models\Utente;

class DashboardAdminController extends Controller
{
  public function getDashboard($req, $res, $args)
  {
    $idAdmin = $this->auth->user()->id;
    $data = [
      'admin' => Utente::where('id', $idAdmin)->firstOrFail(),
    ];
    return $this->view->render($res, 'admin/dashboard.twig', $data);
  }
}