<?php

namespace WebPranzo\Controllers\Utente;
use WebPranzo\Controllers\Controller;
use WebPranzo\Models\Ordine;
use WebPranzo\Models\Notifica;
use WebPranzo\Models\StatoSpedizione;

class OrdiniController extends Controller
{
  public function getOrdiniFornitore($req, $res, $args)
  {
    $data = [
      'ordini' => $this->listOrdini(),
    ];
    return $this->view->render($res, 'fornitore/ordini.twig', $data);
  }

  public function getOrdiniCliente($req, $res, $args)
  {
    $data = [
      'ordini' => $this->listOrdini(),
    ];
    return $this->view->render($res, 'cliente/ordini.twig', $data);
  }

  public function getOrdiniFattorino($req, $res, $args)
  {
    $data = [
      'ordini'          => $this->listOrdini(),
      'statiSpedizione' => StatoSpedizione::all(),
    ];
    return $this->view->render($res, 'fattorino/ordini.twig', $data);
  }

  private function listOrdini()
  {
    $idUtente = $this->auth->user()->id;
    switch ($this->auth->user()->tipoUtente->nome) {
      case 'Cliente':
        $q = Ordine::where('id_cliente', '=', $idUtente);
        break;
      case 'Fornitore':
        $q = Ordine::where('id_fornitore', '=', $idUtente);
        break;
      case 'Fattorino':
        $q = Ordine::where('evaso', '=', '1');
        break;
      default:
        break;
    }
    $ordini = $q->get();
    return $ordini;
  }

  public function postOrdiniEvadere($req, $res, $args)
  {
    $idFornitore = $this->auth->user()->id;

    $idOrdine = $args['id'];
    $ordine = Ordine::find($idOrdine);
    $ordine->evaso = '1';
    $ordine->save();

    $notifica = Notifica::create([
      'data_creazione' => date("Y-m-d H:i:s"),
      'titolo'         => "Ordine evaso",
      'testo'          => "Gentile cliente, il suo ordine è stato evaso dal fornitore. A breve le sarà consegnato presso il luogo da lei prescelto. Speriamo di aver soddisfatto le sue richieste e attendiamo il suo prossimo ordine. Buona gionata!!!",
      'id_fornitore'   => $idFornitore,
      'id_cliente'     => $ordine->id_cliente,
      'id_ordine'      => $ordine->id,
    ]);

    $data = ['status' => 'true'];
    return $res->withJson($data);
  }

  public function postChangeStatoSpedizioneOrdine($req, $res, $args)
  {
    $idFornitore = $this->auth->user()->id;

    $idOrdine = $args['idOrdine'];
    $ordine = Ordine::find($idOrdine);
    $idStato = $args['idStato'];
    $stato = StatoSpedizione::find($idStato);
    if ($stato->nome == 'Arrivata') {
      $ordine->dettagliSpedizione->consegnata = '1';
      $ordine->dettagliSpedizione->data_arrivo = date("Y-m-d H:i:s");
    }
    $ordine->dettagliSpedizione->id_stato = $stato->id;
    $ordine->dettagliSpedizione->save();

    $titolo = "";
    $messaggio = "";

    switch ($stato->nome) {
      case 'In elaborazione':
        $titolo = "Ordine è in laborazione";
        $messaggio = "Gentile cliente, il suo ordine è in laborazione presso uno dei nostri speditori.";
        break;
      case 'Spedito':
        $titolo = "Ordine è spedito";
        $messaggio = "Gentile cliente, il suo ordine è spedito.";
        break;
      case 'In arrivo':
        $titolo = "Ordine è in arrivo";
        $messaggio = "Gentile cliente, il suo ordine è in arrivo oggi.";
        break;
      case 'Arrivata':
        $titolo = "Ordine è arrivato";
        $messaggio = "Gentile cliente, il suo ordine è stato arrivato.";
        break;
      default:
        break;
    }

    $notifica = Notifica::create([
      'data_creazione' => date("Y-m-d H:i:s"),
      'titolo'         => $titolo,
      'testo'          => $messaggio,
      'id_fornitore'   => $idFornitore,
      'id_cliente'     => $ordine->id_cliente,
      'id_ordine'      => $ordine->id,
    ]);

    $data = ['status' => 'true'];
    $this->flash->addMessage('info', 'Stato spedizione cambiato');
    return $res->withJson($data);
  }

  public function getDettagliOrdineCliente($req, $res, $args)
  {
    if (empty($args['id'])) {
      return $res->withRedirect($this->router->pathFor('cliente.messaggi'));
    }

    $ordine = Ordine::find($args['id']);
    $tipoPagamento = $ordine->dettagliPagamento->tipoPagamento;
    $sconto = $ordine->sconto;
    $totale = $ordine->totale;

    $tipoSpedizione = $ordine->dettagliSpedizione->tipoSpedizione;

    $data = [
      'piatti' => $ordine->piatti,
      'indirizzo_spedizione' => [
        'luogo' => $ordine->dettagliSpedizione->luogoConsegna->nome,
      ],
      'pagamento'  => $tipoPagamento->nome,
      'spedizione' => $tipoSpedizione,
      'sconto'     => $sconto,
      'totale'     => $totale,
    ];
    return $this->view->render($res, 'cliente/dettagliOrdine.twig', $data);
  }
}
