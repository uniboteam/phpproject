<?php

namespace WebPranzo\Controllers\Utente;
use WebPranzo\Controllers\Controller;
use WebPranzo\Models\Utente;
use WebPranzo\Models\Citta;
use WebPranzo\Models\TipoCucina;
use WebPranzo\Models\TipoUtente;
use WebPranzo\Models\Sconto;
use WebPranzo\Models\OrarioSettimanale;
use Respect\Validation\Validator as v;

class ProfiloController extends Controller
{
  public function getProfilo($req, $res)
  {
    $idUtente = $this->auth->user()->id;
    $utente = Utente::where('id', $idUtente)->firstOrFail();
    $tipoUtente = TipoUtente::find($utente->tipo);
    $data = [
      'utente'     => $utente,
      'tipoUtente' => $tipoUtente,
      'citta'      => Citta::all(),
      'tipiCucina' => TipoCucina::all(),
      'sconti'     => Sconto::all(),
    ];
    switch ($tipoUtente->nome) {
      case 'Cliente':
        return $this->view->render($res, 'utente/profiloCliente.twig', $data);
        break;
      case 'Fornitore':
        return $this->view->render($res, 'utente/profiloFornitore.twig', $data);
        break;
      case 'Amministratore':
        return $this->view->render($res, 'utente/profiloAdmin.twig', $data);
        break;
      case 'Fattorino':
        return $this->view->render($res, 'utente/profiloFattorino.twig', $data);
        break;
      default:
      break;
    }
    return $res->withRedirect($this->router->pathFor('home'));
  }

  public function postProfilo($req, $res)
  {
    $idUtente = $this->auth->user()->id;
    $utente = Utente::where('id', $idUtente)->firstOrFail();
    $tipoUtente = TipoUtente::find($utente->tipo);
    
    switch ($tipoUtente->nome) {
      case 'Cliente':
        return $this->handlePostCliente($req, $res);
        break;
      case 'Fornitore':
        return $this->handlePostFornitore($req, $res);
        break;
      case 'Amministratore':
        return $this->handlePostAdmin($req, $res);
        break;
      case 'Fattorino':
        return $this->handlePostFattorino($req, $res);
        break;
      default:
      break;
    }
    return $res->withRedirect($this->router->pathFor('home'));
  }

  private function handlePostCliente($req, $res)
  {
    $idCliente = $this->auth->user()->id;
    $cliente   = Utente::where('id', $idCliente)->firstOrFail();

    if (empty($req->getParam('cambiapw'))) {
      $validation = $this->validator->validate($req, [
        'nome'         => v::notEmpty(),
        'cognome'      => v::notEmpty(),
        'telefono'     => v::notEmpty(),
        'citta'        => v::numeric()->notEmpty(),
      ]);

      if ($validation->failed()) {
        return $res->withRedirect($this->router->pathFor('utente.profilo'));
      }
    } else {
      $validation = $this->validator->validate($req, [
        'nome'         => v::notEmpty(),
        'cognome'      => v::notEmpty(),
        'telefono'     => v::notEmpty(),
        'citta'        => v::numeric()->notEmpty(),
        'password_old' => v::noWhitespace()->notEmpty()->matchesPassword($this->auth->user()->password),
        'password'     => v::noWhitespace()->notEmpty(),
        'repassword'   => v::noWhitespace()->notEmpty()->equals($req->getParam('password')),
      ]);
      
      if ($validation->failed()) {
        return $res->withRedirect($this->router->pathFor('utente.profilo'));
      }

      $this->auth->user()->setPassword($req->getParam('password'));
    }

    $cliente->nome               = $req->getParam('nome');
    $cliente->cognome            = $req->getParam('cognome');
    $cliente->telefono_cellulare = $req->getParam('telefono');
    $cliente->indirizzo_citta    = $req->getParam('citta');
    $cliente->save();

    $this->flash->addMessage('info', 'Profilo salvato correttamente');

    return $res->withRedirect($this->router->pathFor('home'));
  }

  private function handlePostFornitore($req, $res)
  {
    $idFornitore = $this->auth->user()->id;
    $fornitore   = Utente::where('id', $idFornitore)->firstOrFail();

    if (empty($req->getParam('cambiapw'))) {
      $validation = $this->validator->validate($req, [
        'nomeAttivita' => v::notEmpty(),
        'via'          => v::notEmpty(),
        'numero'       => v::notEmpty(),
        'citta'        => v::numeric()->notEmpty(),
        'telefono'     => v::notEmpty(),
        'tipoCucina'   => v::numeric()->notEmpty(),
      ]);

      if ($validation->failed()) {
        return $res->withRedirect($this->router->pathFor('utente.profilo'));
      }

    } else {
      $validation = $this->validator->validate($req, [
        'nomeAttivita' => v::notEmpty(),
        'via'          => v::notEmpty(),
        'numero'       => v::notEmpty(),
        'citta'        => v::numeric()->notEmpty(),
        'telefono'     => v::notEmpty(),
        'tipoCucina'   => v::numeric()->notEmpty(),
        'password_old' => v::noWhitespace()->notEmpty()->matchesPassword($this->auth->user()->password),
        'password'     => v::noWhitespace()->notEmpty(),
        'repassword'   => v::noWhitespace()->notEmpty()->equals($req->getParam('password')),
      ]);
      
      if ($validation->failed()) {
        return $res->withRedirect($this->router->pathFor('utente.profilo'));
      }

      $this->auth->user()->setPassword($req->getParam('password'));
    }

    $fornitore->nome_attivita       = $req->getParam('nomeAttivita');
    $fornitore->indirizzo_via       = $req->getParam('via');
    $fornitore->indirizzo_numero    = $req->getParam('numero');
    $fornitore->indirizzo_citta     = $req->getParam('citta');
    $fornitore->telefono_cellulare  = $req->getParam('telefono');
    $fornitore->logo                = $req->getParam('logo');
    $fornitore->id_specializzazione = $req->getParam('tipoCucina');
    $fornitore->id_sconto           = $req->getParam('sconto');
    $fornitore->save();

    $this->flash->addMessage('info', 'Profilo salvato correttamente');
    return $res->withRedirect($this->router->pathFor('fornitore.dashboard'));
  }

  private function handlePostAdmin($req, $res)
  {
    $idAdmin = $this->auth->user()->id;
    $admin   = Utente::where('id', $idAdmin)->firstOrFail();

    if (empty($req->getParam('cambiapw'))) {
      $validation = $this->validator->validate($req, [
        'nome'         => v::notEmpty(),
        'cognome'      => v::notEmpty(),
        'citta'        => v::numeric()->notEmpty(),
      ]);

      if ($validation->failed()) {
        return $res->withRedirect($this->router->pathFor('utente.profilo'));
      }
    } else {
      $validation = $this->validator->validate($req, [
        'nome'         => v::notEmpty(),
        'cognome'      => v::notEmpty(),
        'citta'        => v::numeric()->notEmpty(),
        'password_old' => v::noWhitespace()->notEmpty()->matchesPassword($this->auth->user()->password),
        'password'     => v::noWhitespace()->notEmpty(),
        'repassword'   => v::noWhitespace()->notEmpty()->equals($req->getParam('password')),
      ]);
      
      if ($validation->failed()) {
        return $res->withRedirect($this->router->pathFor('utente.profilo'));
      }

      $this->auth->user()->setPassword($req->getParam('password'));
    }

    $admin->nome               = $req->getParam('nome');
    $admin->cognome            = $req->getParam('cognome');
    $admin->indirizzo_citta    = $req->getParam('citta');
    $admin->save();

    $this->flash->addMessage('info', 'Profilo salvato correttamente');

    return $res->withRedirect($this->router->pathFor('home'));
  }

  private function handlePostFattorino($req, $res)
  {
    $idFattorino = $this->auth->user()->id;
    $admin   = Utente::where('id', $idFattorino)->firstOrFail();

    if (!empty($req->getParam('cambiapw'))) {
      $validation = $this->validator->validate($req, [
        'password_old' => v::noWhitespace()->notEmpty()->matchesPassword($this->auth->user()->password),
        'password'     => v::noWhitespace()->notEmpty(),
        'repassword'   => v::noWhitespace()->notEmpty()->equals($req->getParam('password')),
      ]);
      
      if ($validation->failed()) {
        return $res->withRedirect($this->router->pathFor('utente.profilo'));
      }

      $this->auth->user()->setPassword($req->getParam('password'));
    }
    
    $admin->save();

    $this->flash->addMessage('info', 'Profilo salvato correttamente');

    return $res->withRedirect($this->router->pathFor('home'));
  }

  public function getOrario($req, $res)
  {
    $fornitore = $this->auth->user();
    $data = [
      'utente' => $fornitore,
    ];

    return $this->view->render($res, 'utente/orarioSettimanale.twig', $data);
  }

  public function postOrario($req, $res)
  {
    $idUtente = $this->auth->user()->id;
    $utente = Utente::find($idUtente);

    if (empty($utente->orarioSettimanale)) {
      $orario = OrarioSettimanale::create([
        'lunedi_dal'    => $req->getParam('lunedi_dal'),
        'lunedi_al'     => $req->getParam('lunedi_al'),
        'martedi_dal'   => $req->getParam('martedi_dal'),
        'martedi_al'    => $req->getParam('martedi_al'),
        'mercoledi_dal' => $req->getParam('mercoledi_dal'),
        'mercoledi_al'  => $req->getParam('mercoledi_al'),
        'giovedi_dal'   => $req->getParam('giovedi_dal'),
        'giovedi_al'    => $req->getParam('giovedi_al'),
        'venerdi_dal'   => $req->getParam('venerdi_dal'),
        'venerdi_al'    => $req->getParam('venerdi_al'),
        'sabato_dal'    => $req->getParam('sabato_dal'),
        'sabato_al'     => $req->getParam('sabato_al'),
        'domenica_dal'  => $req->getParam('domenica_dal'),
        'domenica_al'   => $req->getParam('domenica_al'),
      ]);
      $utente->orario_id = $orario->id;
      $utente->save();
    } else {
      $orario = $utente->orarioSettimanale;
      $orario->lunedi_dal    = $req->getParam('lunedi_dal');
      $orario->lunedi_al     = $req->getParam('lunedi_al');
      $orario->martedi_dal   = $req->getParam('martedi_dal');
      $orario->martedi_al    = $req->getParam('martedi_al');
      $orario->mercoledi_dal = $req->getParam('mercoledi_dal');
      $orario->mercoledi_al  = $req->getParam('mercoledi_al');
      $orario->giovedi_dal   = $req->getParam('giovedi_dal');
      $orario->giovedi_al    = $req->getParam('giovedi_al');
      $orario->venerdi_dal   = $req->getParam('venerdi_dal');
      $orario->venerdi_al    = $req->getParam('venerdi_al');
      $orario->sabato_dal    = $req->getParam('sabato_dal');
      $orario->sabato_al     = $req->getParam('sabato_al');
      $orario->domenica_dal  = $req->getParam('domenica_dal');
      $orario->domenica_al   = $req->getParam('domenica_al');
      $orario->save();
    }
    
    $this->flash->addMessage('info', 'Orario settimanale salvato');
    return $res->withRedirect($this->router->pathFor('utente.orario'));
  }

  public function getElencoUtenti($req, $res)
  {
    $idUtente = $this->auth->user()->id;
    $utenti = Utente::where('id', '!=', $idUtente)->get();
    
    $data = [
      'utenti' => $utenti,
    ];
    return $this->view->render($res, 'admin/elencoUtenti.twig', $data);
  }

  public function getDettagliUtente($req, $res, $args)
  {
    $idUtente = $args['id'];
    $utente = Utente::find($idUtente);
    
    $data = [
      'utente'     => $utente,
      'citta'      => Citta::all(),
      'tipiCucina' => TipoCucina::all(),
      'tipiUtente' => TipoUtente::all(),
      'sconti'     => Sconto::all(),
    ];
    return $this->view->render($res, 'admin/dettagliUtente.twig', $data);
  }

  public function postDettagliUtente($req, $res, $args)
  {
    $idUtente = $args['id'];
    $utente = Utente::where('id', $idUtente)->firstOrFail();
    
    if (empty($req->getParam('cambiapw'))) {
      $rules = [];
      if ($req->getParam('email') != $utente->email) {
        $rules = [
          'email' => v::noWhitespace()->notEmpty()->email()->emailAvailable(),
        ];
      }
      $validation = $this->validator->validate($req, $rules);
      
      if ($validation->failed()) {
        return $res->withRedirect($this->router->pathFor('admin.dettagliUtente', ['id' => $idUtente]));
      }
    } else {
      $rules = [];
      if ($req->getParam('email') != $utente->email) {
        $rules = [
          'email'        => v::noWhitespace()->notEmpty()->email()->emailAvailable(),
          'citta'        => v::numeric()->notEmpty(),
          'tipoCucina'   => v::numeric()->notEmpty(),
          'password_old' => v::noWhitespace()->notEmpty()->matchesPassword($this->auth->user()->password),
          'password'     => v::noWhitespace()->notEmpty(),
          'repassword'   => v::noWhitespace()->notEmpty()->equals($req->getParam('password')),
        ];
      } else {
        $rules = [
          'citta'        => v::numeric()->notEmpty(),
          'tipoCucina'   => v::numeric()->notEmpty(),
          'password_old' => v::noWhitespace()->notEmpty()->matchesPassword($this->auth->user()->password),
          'password'     => v::noWhitespace()->notEmpty(),
          'repassword'   => v::noWhitespace()->notEmpty()->equals($req->getParam('password')),
        ];
      }
      $validation = $this->validator->validate($req, $rules);
      
      if ($validation->failed()) {
        return $res->withRedirect($this->router->pathFor('admin.dettagliUtente', ['id' => $idUtente]));
      }

      $utente->setPassword($req->getParam('password'));
    }

    $utente->nome                = $req->getParam('nome');
    $utente->cognome             = $req->getParam('cognome');
    $utente->telefono_cellulare  = $req->getParam('telefono');
    $utente->email               = $req->getParam('email');
    $utente->indirizzo_via       = $req->getParam('via');
    $utente->indirizzo_numero    = $req->getParam('numero');
    $utente->indirizzo_citta     = $req->getParam('citta');
    $utente->nome_attivita       = $req->getParam('nomeAttivita');
    $utente->telefono_cellulare  = $req->getParam('telefono');
    $utente->logo                = $req->getParam('logo');
    $utente->id_specializzazione = $req->getParam('tipoCucina');
    $utente->id_sconto           = $req->getParam('sconto');
    $utente->tipo                = $req->getParam('tipoUtente');
    $utente->save();

    $this->flash->addMessage('info', 'Profilo salvato correttamente');

    return $res->withRedirect($this->router->pathFor('admin.elencoUtenti'));
  }

  public function getIndirizzoUtente($req, $res, $args)
  {
    $idUtente = $args['id'];
    $utente = Utente::find($idUtente);
    $indirizzo = [];
    
    if (!empty($utente)) {
      $indirizzo = [
        'via'    => $utente->indirizzo_via,
        'numero' => $utente->indirizzo_numero,
        'citta'  => $utente->citta->nome,
      ];
    }
    
    $data = ['indirizzo' => $indirizzo];
    return $res->withJson($data);
  } 
}