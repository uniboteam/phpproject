<?php

namespace WebPranzo\Controllers\Utente;
use Illuminate\Database\Query\Expression as raw;
use WebPranzo\Controllers\Controller;
use WebPranzo\Models\Utente;
use WebPranzo\Models\Ordine;
use WebPranzo\Models\Notifica;
use WebPranzo\Models\Feedback;
use WebPranzo\Models\TestoFeedback;

class MessaggiController extends Controller
{
  public function getMessaggiCliente($req, $res, $args)
  {
    $idUtente = $this->auth->user()->id;
    $data = [
      'messaggi' => Notifica::where('id_cliente', '=', $idUtente)->orderBy('data_creazione', 'desc')->get(),
    ];
    return $this->view->render($res, 'cliente/messaggi.twig', $data);
  }
  
  public function getOrdiniCliente($req, $res, $args)
  {
    $data = [
      'ordini' => $this->listOrdini(),
    ];
    return $this->view->render($res, 'cliente/ordini.twig', $data);
  }

  private function listOrdini()
  {
    $idUtente = $this->auth->user()->id;
    switch ($this->auth->user()->tipoUtente->nome) {
      case 'Cliente':
        $q = Ordine::where('id_cliente', '=', $idUtente);
        break;
      case 'Fornitore':
        $q = Ordine::where('id_fornitore', '=', $idUtente);
        break;
      default:
        break;
    }
    $ordini = $q->get();
    return $ordini;
  }

  public function postOrdiniEvadere($req, $res, $args)
  {
    $idOrdine = $args['id'];
    $ordine = Ordine::find($idOrdine);
    $ordine->evaso = '1';
    $ordine->save();

    $data = ['status' => 'true'];
    $newResponse = $res->withJson($data);
  }

  public function getFeedbackCliente($req, $res, $args)
  {
    $ordine = Ordine::find($args['id']);
    if (count($ordine->feedback) >= 1) {
      $this->flash->addMessage('error', 'Hai gia valutato questo ordine');
      return $res->withRedirect($this->router->pathFor('cliente.ordini'));
    }
    
    $data = [
      'ordine' => $ordine,
    ];
    return $this->view->render($res, 'cliente/feedback.twig', $data);
  }

  public function postFeedbackCliente($req, $res, $args)
  {
    $idOrdine = $args['id'];
    $ordine = Ordine::find($idOrdine);
    $mapPiatti = [];
    $testoFeedback = new TestoFeedback();

    foreach($req->getParams() as $key=>$value)
    if(count(explode('voto_piatto_', $key)) > 1){
      $mapPiatti[explode('_', $key)[2]] = $value;
    }
    if (!empty($req->getParam('feedback'))) {
      $testoFeedback = TestoFeedback::create([
        'testo' => $req->getParam('feedback'),
      ]);
    }
    
    foreach ($mapPiatti as $kPiatto => $vPiatto) {
      $feedback = Feedback::create([
        'id_ordine'      => $ordine->id,
        'id_cliente'     => $ordine->cliente->id,
        'id_piatto'      => $kPiatto,
        'voto'           => $vPiatto,
        'data_creazione' => date("Y-m-d H:i:s"),
      ]);
      if (!empty($req->getParam('feedback'))) {
        $feedback->descrizione_id = $testoFeedback->id;
        $feedback->save();
      }
    }

    $this->flash->addMessage('info', 'Grazie per la tua valutazione');
    return $res->withRedirect($this->router->pathFor('cliente.ordini'));
  }

  public function getFeedbackFornitore($req, $res, $args)
  {
    $idFornitore = $this->auth->user()->id;
    
    $data = [
      'feedbacks' => $this->getTuttiFeedbackFornitore($idFornitore),
    ];
    return $this->view->render($res, 'fornitore/feedback.twig', $data);
  }

  private function getTuttiFeedbackFornitore($idFornitore)
  {
    // Prendo tutti i feedback di questo fornitore con la colonna della media dei voti arrotondato
    return Feedback::whereHas('ordine', function($q) use ($idFornitore) {
      $q->where('id_fornitore', '=', $idFornitore);
    })
    ->select('id', 'descrizione_id', 'id_ordine', 'data_creazione', new raw("round(avg(`voto`)) as votoMedio"), 'id_cliente', 'id_piatto')
    ->groupBy('id_ordine')
    ->get();
  }

  public function getFeedbackDelFornitore($req, $res, $args)
  {
    $idFornitore = $args['id'];
    
    $data = [
      'fornitore' => Utente::find($idFornitore),
      'feedbacks' => $this->getTuttiFeedbackFornitore($idFornitore),
    ];
    return $this->view->render($res, 'fornitore/feedbackForPublic.twig', $data);
  }
}