<?php

namespace WebPranzo\Controllers\Fornitore;
use WebPranzo\Controllers\Controller;
use WebPranzo\Models\Utente;
use WebPranzo\Models\Listino;
use WebPranzo\Models\Piatto;
use WebPranzo\Models\Ingrediente;
use WebPranzo\Models\CategoriaIngrediente;
use WebPranzo\Models\DettagliListino;
use WebPranzo\Models\DettagliPiatto;
use Respect\Validation\Validator as v;

class ListiniController extends Controller
{
  public function getListini($req, $res)
  {
    $idFornitore = $this->auth->user()->id;
    $data = [
      'fornitore' => Utente::where('id', $idFornitore)->firstOrFail(),
      'listini' => Listino::where('id_fornitore', $idFornitore)->get(),
    ];
    return $this->view->render($res, 'fornitore/listini.twig', $data);
  }

  public function getListino($req, $res, $args)
  {
    $idFornitore = $this->auth->user()->id;

    $listino = new Listino();
    $piatti = [];
    if ($args['id'] != '0') {
      $listino = Listino::find($args['id']);
      $piatti = $listino->piatti;
    }
    $data = [
      'isNuovoListino' => $args['id'] == '0' ? true: false,
      'idListino'      => $args['id'],
      'listino'        => $listino,
      'piatti'         => $piatti,
    ];
    return $this->view->render($res, 'fornitore/listino.twig', $data);
  }

  public function postListino($req, $res, $args)
  {
    $idFornitore = $this->auth->user()->id;
    $path = $req->getUri()->getPath();
    $idListino = \explode('/', $path)[3];

    $validation = $this->validator->validate($req, [
      'nome' => v::notEmpty(),
    ]);

    if ($validation->failed()) {
      return $res->withRedirect($this->router->pathFor('fornitore.listino', ['id' => $idListino]));
    }

    if ($idListino == '0') {
      $listino = Listino::create([
        'nome'         => $req->getParam('nome'),
        'attivo'       => empty($req->getParam('attivo')) ? '0': '1',
        'id_fornitore' => $idFornitore,
      ]);
      return $res->withRedirect($this->router->pathFor('fornitore.listino', ['id' => $listino->getKey()]));
    } else {
      $listino = Listino::find($req->getParam('idListino'));
      $listino->nome = $req->getParam('nome');
      $listino->attivo = empty($req->getParam('attivo')) ? '0' : '1';
      $listino->save();
      return $res->withRedirect($this->router->pathFor('fornitore.listini'));
    }

  }

  public function getPiatto($req, $res, $args)
  {
    $piatto = new Piatto();
    $ingredienti = [];
    if ($args['idPiatto'] != '0') {
      $piatto = Piatto::find($args['idPiatto']);
      $ingredienti = $piatto->ingredienti;
    }

    $data = [
      'isNuovoPiatto' => $args['idPiatto'] == '0' ? true: false,
      'idListino'     => $args['idListino'],
      'idPiatto'      => $args['idPiatto'],
      'piatto'        => $piatto,
      'ingredienti'   => $ingredienti,
    ];
    return $this->view->render($res, 'fornitore/piatto.twig', $data);
  }

  public function postPiatto($req, $res, $args)
  {
    $path = $req->getUri()->getPath();
    $pathExpl = \explode('/', $path);
    $idListino = \explode('/', $path)[3];
    $idPiatto = \explode('/', $path)[5];

    $validation = $this->validator->validate($req, [
      'nome'   => v::notEmpty(),
      'prezzo' => v::notEmpty(),
    ]);

    if ($validation->failed()) {
      return $res->withRedirect($this->router->pathFor('fornitore.listino.piatto', ['idListino' => $idListino, 'idPiatto' => $idPiatto]));
    }

    if ($idPiatto == '0') {
      $piatto = Piatto::create([
        'nome'           => $req->getParam('nome'),
        'prezzo'         => $req->getParam('prezzo'),
        'id_tipo_cucina' => '1', // per momento non serve, mettiamo sempre uno
      ]);
      $dettagliListino = DettagliListino::create([
        'id_piatto' => $piatto->getKey(),
        'id_listino' => $idListino,
      ]);
      return $res->withRedirect($this->router->pathFor('fornitore.listino.piatto', ['idListino' => $idListino, 'idPiatto' => $piatto->getKey()]));
    } else {
      $piatto = Piatto::find($req->getParam('idPiatto'));
      $piatto->nome = $req->getParam('nome');
      $piatto->prezzo = $req->getParam('prezzo');
      $piatto->save();

      return $res->withRedirect($this->router->pathFor('fornitore.listino', ['id' => $idListino]));
    }
  }

  public function getIngrediente($req, $res, $args)
  {
    $ingrediente = new Ingrediente();
    if ($args['idIngrediente'] != '0') {
      $ingrediente = Ingrediente::find($args['idIngrediente']);
    }

    $data = [
      'isNuovoIngrediente'   => $args['idIngrediente'] == '0' ? true: false,
      'idListino'            => $args['idListino'],
      'idPiatto'             => $args['idPiatto'],
      'idIngrediente'        => $args['idIngrediente'],
      'ingrediente'          => $ingrediente,
      'categorieIngrediente' => CategoriaIngrediente::all(),
    ];
    return $this->view->render($res, 'fornitore/ingrediente.twig', $data);
  }

  public function postIngrediente($req, $res, $args)
  {
    $path = $req->getUri()->getPath();
    $pathExpl = \explode('/', $path);
    $idListino = \explode('/', $path)[3];
    $idPiatto = \explode('/', $path)[5];
    $idIngrediente = \explode('/', $path)[7];

    $validation = $this->validator->validate($req, [
      'nome' => v::notEmpty(),
    ]);

    if ($validation->failed()) {
      return $res->withRedirect($this->router->pathFor('fornitore.listino.piatto.ingrediente', ['idListino' => $idListino, 'idPiatto' => $idPiatto, 'idIngrediente' => $idIngrediente]));
    }

    if ($idIngrediente == '0') {
      $ingrediente = Ingrediente::create([
        'nome'         => $req->getParam('nome'),
        'id_categoria' => $req->getParam('categoria'),
      ]);
      $dettagliPiatto = DettagliPiatto::create([
        'id_ingrediente' => $ingrediente->getKey(),
        'id_piatto'      => $idPiatto,
      ]);
      return $res->withRedirect($this->router->pathFor('fornitore.listino.piatto', ['idListino' => $idListino, 'idPiatto' => $idPiatto]));
    } else {
      $ingrediente = Ingrediente::find($req->getParam('idIngrediente'));
      $ingrediente->nome = $req->getParam('nome');
      $ingrediente->id_categoria = $req->getParam('categoria');
      $ingrediente->save();
      return $res->withRedirect($this->router->pathFor('fornitore.listino.piatto', ['idListino' => $idListino, 'idPiatto' => $idPiatto]));
    }
  }
}