<?php

namespace WebPranzo\Controllers\Fornitore;
use WebPranzo\Controllers\Controller;
use WebPranzo\Models\Utente;

class DashboardFornitoreController extends Controller
{
  public function getDashboard($req, $res, $args)
  {
    $idFornitore = $this->auth->user()->id;
    $data = [
      'fornitore' => Utente::where('id', $idFornitore)->firstOrFail(),
    ];
    return $this->view->render($res, 'fornitore/dashboard.twig', $data);
  }
}