<?php

namespace WebPranzo\Controllers\Auth;
use WebPranzo\Models\Utente;
use WebPranzo\Models\Citta;
use WebPranzo\Models\TipoUtente;
use WebPranzo\Models\TipoCucina;
use WebPranzo\Controllers\Controller;
use Respect\Validation\Validator as v;

class AuthController extends Controller
{
  public function Logout($req, $res)
  {
    $this->auth->logOut();
    return $res->withRedirect($this->router->pathFor('home'));
  }

  public function getLogin($req, $res)
  {
    return $this->view->render($res, 'auth/login.twig');
  }

  public function postLogin($req, $res)
  {
    $auth = $this->auth->attempt(
      $req->getParam('email'),
      $req->getParam('password')
    );

    if (!$auth) {
      $this->flash->addMessage('error', 'Non puoi loggarsi con questi dati');
      return $res->withRedirect($this->router->pathFor('auth.login'));
    }

    if($this->auth->isCliente()) {
      return $res->withRedirect($this->router->pathFor('cliente.ricerca'));
    } else if ($this->auth->isFornitore()) {
      return $res->withRedirect($this->router->pathFor('fornitore.dashboard'));
    } else if ($this->auth->isAdmin()) {
      return $res->withRedirect($this->router->pathFor('admin.dashboard'));
    } else if ($this->auth->isFattorino()) {
      return $res->withRedirect($this->router->pathFor('fattorino.ordini'));
    } else {
      return $res->withRedirect($this->router->pathFor('home'));
    }
  }

  public function getPreRegistra($req, $res)
  {
    unset($_SESSION['registra']);

    $data = [
      'tipiUtente' => TipoUtente::all(),
      'citta'      => Citta::all(),
    ];
    return $this->view->render($res, 'auth/registrazione.twig', $data);
  }

  public function postPreRegistra($req, $res)
  {
    $validation = $this->validator->validate($req, [
      'email'      => v::noWhitespace()->notEmpty()->email()->emailAvailable(),
      'password'   => v::noWhitespace()->notEmpty(),
      'repassword' => v::noWhitespace()->notEmpty()->equals($req->getParam('password')),
    ]);

    if ($validation->failed()) {
      return $res->withRedirect($this->router->pathFor('auth.registra'));
    }

    $_SESSION['registra'] = [
      'email'    => $req->getParam('email'),
      'tipo'     => $req->getParam('tipoUtente'),
      'password' => $req->getParam('password'),
    ];

    switch (TipoUtente::where('id', $_SESSION['registra']['tipo'])->firstOrFail()->nome) {
      case 'Cliente':
        return $res->withRedirect($this->router->pathFor('auth.registraCliente'));
        break;
      case 'Fornitore':
        return $res->withRedirect($this->router->pathFor('auth.registraFornitore'));
        break;
      case 'Amministratore':
        return $res->withRedirect($this->router->pathFor('auth.registraAmministratore'));
        break;
      case 'Fattorino':
        return $this->handleRegistraFattorino($req, $res);
        break;
      default:
        return $res->withRedirect($this->router->pathFor('auth.registra'));
        break;
    }

  }

  private function handleRegistraFattorino($req, $res)
  {
    $citta = Citta::first();
    $user = Utente::create([
      'email'           => $_SESSION['registra']['email'],
      'password'        => password_hash($_SESSION['registra']['password'], PASSWORD_DEFAULT),
      'indirizzo_citta' => $citta->id,
      'tipo'            => $_SESSION['registra']['tipo'],
    ]);

    $this->flash->addMessage('info', 'Account creato');
    $this->auth->attempt($user->email, $_SESSION['registra']['password']);

    unset($_SESSION['registra']);

    return $res->withRedirect($this->router->pathFor('fattorino.ordini'));
  }

  public function getRegistraCliente($req, $res)
  {
    if (
      empty($_SESSION['registra'])
      || empty($_SESSION['registra']['tipo'])
      || TipoUtente::where('id', $_SESSION['registra']['tipo'])->firstOrFail()->nome != 'Cliente'
    ) {
      unset($_SESSION['registra']);
      return $res->withRedirect($this->router->pathFor('auth.registra'));
    }

    $data = [
      'citta' => Citta::all(),
    ];
    return $this->view->render($res, 'auth/registrazioneCliente.twig', $data);
  }

  public function postRegistraCliente($req, $res)
  {

    $validation = $this->validator->validate($req, [
      'nome'     => v::notEmpty()->alpha(),
      'cognome'  => v::notEmpty()->alpha(),
      'telefono' => v::numeric()->noWhitespace()->notEmpty(),
      'citta'    => v::numeric()->notEmpty(),
    ]);

    if ($validation->failed()) {
      return $res->withRedirect($this->router->pathFor('auth.registraCliente'));
    }

    $user = Utente::create([
      'nome'               => $req->getParam('nome'),
      'cognome'            => $req->getParam('cognome'),
      'email'              => $_SESSION['registra']['email'],
      'password'           => password_hash($_SESSION['registra']['password'], PASSWORD_DEFAULT),
      'telefono_cellulare' => $req->getParam('telefono'),
      'indirizzo_citta'    => $req->getParam('citta'),
      'tipo'               => $_SESSION['registra']['tipo'],
    ]);

    $this->flash->addMessage('info', 'Account creato');
    $this->auth->attempt($user->email, $_SESSION['registra']['password']);

    unset($_SESSION['registra']);

    return $res->withRedirect($this->router->pathFor('cliente.ricerca'));
  }

  public function getRegistraFornitore($req, $res)
  {
    if (
      empty($_SESSION['registra'])
      || empty($_SESSION['registra']['tipo'])
      || TipoUtente::where('id', $_SESSION['registra']['tipo'])->firstOrFail()->nome != 'Fornitore'
    ) {
      unset($_SESSION['registra']);
      return $res->withRedirect($this->router->pathFor('auth.registra'));
    }

    $data = [
      'citta' => Citta::all(),
      'tipiCucina' => TipoCucina::all(),
    ];
    return $this->view->render($res, 'auth/registrazioneFornitore.twig', $data);
  }

  public function postRegistraFornitore($req, $res)
  {

    $validation = $this->validator->validate($req, [
      'nomeAttivita' => v::notEmpty(),
      'via'          => v::notEmpty(),
      'numero'       => v::notEmpty(),
      'citta'        => v::numeric()->notEmpty(),
      'telefono'     => v::notEmpty(),
      'tipoCucina'   => v::numeric()->notEmpty(),
    ]);

    if ($validation->failed()) {
      return $res->withRedirect($this->router->pathFor('auth.registraFornitore'));
    }

    $user = Utente::create([
      'email'               => $_SESSION['registra']['email'],
      'password'            => password_hash($_SESSION['registra']['password'], PASSWORD_DEFAULT),
      'nome_attivita'       => $req->getParam('nomeAttivita'),
      'indirizzo_via'       => $req->getParam('via'),
      'indirizzo_numero'    => $req->getParam('numero'),
      'indirizzo_citta'     => $req->getParam('citta'),
      'telefono_cellulare'  => $req->getParam('telefono'),
      'logo'                => $req->getParam('logo'),
      'tipo'                => $_SESSION['registra']['tipo'],
      'id_specializzazione' => $req->getParam('tipoCucina'),
    ]);

    $this->flash->addMessage('info', 'Account creato');
    $this->auth->attempt($user->email, $_SESSION['registra']['password']);

    unset($_SESSION['registra']);

    return $res->withRedirect($this->router->pathFor('fornitore.dashboard'));
  }

  public function getRegistraAmministratore($req, $res)
  {
    if (
      empty($_SESSION['registra'])
      || empty($_SESSION['registra']['tipo'])
      || TipoUtente::where('id', $_SESSION['registra']['tipo'])->firstOrFail()->nome != 'Amministratore'
    ) {
      unset($_SESSION['registra']);
      return $res->withRedirect($this->router->pathFor('auth.registra'));
    }

    $data = [
      'citta' => Citta::all(),
    ];
    return $this->view->render($res, 'auth/registrazioneAmministratore.twig', $data);
  }

  public function postRegistraAmministratore($req, $res)
  {
    $validation = $this->validator->validate($req, [
      'nome'    => v::notEmpty()->alpha(),
      'cognome' => v::notEmpty()->alpha(),
    ]);

    if ($validation->failed()) {
      return $res->withRedirect($this->router->pathFor('auth.registraAmministratore'));
    }

    $user = Utente::create([
      'nome'            => $req->getParam('nome'),
      'cognome'         => $req->getParam('cognome'),
      'email'           => $_SESSION['registra']['email'],
      'indirizzo_citta' => $req->getParam('citta'),
      'password'        => password_hash($_SESSION['registra']['password'], PASSWORD_DEFAULT),
      'tipo'            => $_SESSION['registra']['tipo'],
    ]);

    $this->flash->addMessage('info', 'Account creato');
    $this->auth->attempt($user->email, $_SESSION['registra']['password']);

    unset($_SESSION['registra']);

    return $res->withRedirect($this->router->pathFor('admin.dashboard'));
  }

}
