<?php

namespace WebPranzo\Controllers;
use WebPranzo\Models\TipoUtente;

class HomeController extends Controller
{
  public function index($req, $res)
  {
    return $this->view->render($res, 'home.twig');
  }
}
