<?php

namespace WebPranzo\Controllers\Cliente;
use WebPranzo\Models\Utente;
use WebPranzo\Controllers\Controller;
use Respect\Validation\Validator as v;

class InfoFornitoreController extends Controller
{
  public function getFornitore($req, $res, $args)
  {
    $idFornitore = $args['id'];
    $fornitore = Utente::where('id', $idFornitore)->firstOrFail();
    $numRecensioni = 0;
    foreach ($fornitore->ordini as $o) {
      $numRecensioni += \count($o->distinctFeedback);
    }
    $data = [
      'fornitore' => $fornitore,
      'numRecensioni' => $numRecensioni,
    ];
    return $this->view->render($res, 'cliente/infoFornitore.twig', $data);
  }

  public function getRisultatoRicerca($req, $res)
  {
    $data = [
      'fornitori' => $this->getFornitori(),
    ];
    return $this->view->render($res, 'cliente/risultatoRicerca.twig', $data);
  }

  private function getFornitori($filtro = null)
  {
    $tipoFornitore = TipoUtente::where('nome', 'Fornitore')->firstOrFail();
    $idTipoFornitore = $tipoFornitore->id;
    $fornitori = Utente::where('tipo', $idTipoFornitore)->get();
    return $fornitori;
  }
}