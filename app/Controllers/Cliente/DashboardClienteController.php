<?php

namespace WebPranzo\Controllers\Cliente;
use WebPranzo\Controllers\Controller;
use WebPranzo\Models\Utente;

class DashboardClienteController extends Controller
{
  public function getDashboard($req, $res, $args)
  {
    $idCliente = $this->auth->user()->id;
    $data = [
      'cliente' => Utente::where('id', $idCliente)->firstOrFail(),
    ];
    return $this->view->render($res, 'cliente/dashboard.twig', $data);
  }
}