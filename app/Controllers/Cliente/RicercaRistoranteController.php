<?php

namespace WebPranzo\Controllers\Cliente;
use Illuminate\Database\Query\Expression as raw;
use WebPranzo\Models\Utente;
use WebPranzo\Models\TipoUtente;
use WebPranzo\Models\TipoCucina;
use WebPranzo\Models\Feedback;
use WebPranzo\Models\Sconto;
use WebPranzo\Controllers\Controller;
use Respect\Validation\Validator as v;

class RicercaRistoranteController extends Controller
{
  public function getRicerca($req, $res)
  {
    return $this->view->render($res, 'cliente/ricerca.twig');
  }

  public function postRicerca($req, $res)
  {
    $_SESSION['ricerca'] = [
      'chiave' => $req->getParam('ristorante'),
    ];
    
    return $res->withRedirect($this->router->pathFor('cliente.ricerca.risultato'));
  }
  
  public function getRisultatoRicerca($req, $res)
  {
    $data = [
      'fornitori'       => $this->getFornitori($_SESSION['ricerca']),
      'tipiCucina'      => TipoCucina::all(),
      'sconti'          => Sconto::all(),
      'filtriApplicati' => []
    ];
    // Se ci sono i filtri, li passo alla view per la preselezione
    if (!empty($_SESSION['ricerca']['filtri'])) {
      $data['filtriApplicati'] = $_SESSION['ricerca']['filtri'];
    }
    return $this->view->render($res, 'cliente/risultatoRicerca.twig', $data);
  }
  
  private function getFornitori($filtro)
  {
    $nomeAttivita = $filtro['chiave'];
    $tipoFornitore = TipoUtente::where('nome', 'Fornitore')->firstOrFail();
    $idTipoFornitore = $tipoFornitore->id;
    
    $q = Utente::where('tipo', $idTipoFornitore);
    if (!empty($nomeAttivita)) {
      $q->where('nome_attivita', 'like', "%$nomeAttivita%");
    }
    
    if (isset($filtro['filtri']['cucina'])) {
      $q = $q->whereIn('id_specializzazione', $filtro['filtri']['cucina']);
    }

    if (isset($filtro['filtri']['sconti'])) {
      $q = $q->whereIn('id_sconto', $filtro['filtri']['sconti']);
    }
    
    $fornitori = $q->get();
    foreach ($fornitori as $f) {
      $voti = $this->getMediaVotiFornitore($f->id);
      $media = 0;
      if (\count($voti)) {
        $somma = 0;
        foreach ($voti as $voto) {
          $somma += $voto->votoMedio;
        }
        $media = (int)(\round($somma / \count($voti)));
      }
      $f->setAttribute('mediaVoti', $media);
    }

    if (isset($filtro['filtri']['punteggio'])) {
      $fornitori = $fornitori->filter(function ($item) use ($filtro) {
        return $item->mediaVoti >= (int)$filtro['filtri']['punteggio'];
      });
    }
    
    return $fornitori;
  }

  private function getMediaVotiFornitore($idFornitore)
  {
    // Prendo tutti i feedback di questo fornitore con la colonna della media dei voti arrotondato
    return Feedback::whereHas('ordine', function($q) use ($idFornitore) {
      $q->where('id_fornitore', '=', $idFornitore);
    })
    ->select(new raw("round(avg(`voto`)) as votoMedio"))
    ->groupBy('id_ordine')
    ->get('votoMedio');
  }
  
  public function getFiltriRicerca($req, $res)
  {
    $data = [
      'tipiCucina' => TipoCucina::all(),
    ];
    return $this->view->render($res, 'cliente/filtriRicerca.twig', $data);
  }

  public function postFiltriRicerca($req, $res)
  {
    if ($req->getParam('rimuoviFiltri') == '1') {
      $_SESSION['ricerca']['filtri'] = [];
    } else {
      $_SESSION['ricerca']['filtri'] = [
        'cucina'            => $req->getParam('cucina'),
        'sconti'            => $req->getParam('sconti'),
        'punteggio'         => $req->getParam('punteggio'),
      ];
    }

    return $res->withRedirect($this->router->pathFor('cliente.ricerca.risultato'));
  }
}