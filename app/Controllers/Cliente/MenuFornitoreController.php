<?php

namespace WebPranzo\Controllers\Cliente;
use WebPranzo\Models\Utente;
use WebPranzo\Models\Menu;
use WebPranzo\Models\DettagliMenu;
use WebPranzo\Models\Piatto;
use WebPranzo\Models\Ingrediente;
use WebPranzo\Models\CategoriaIngrediente;
use WebPranzo\Controllers\Controller;
use Respect\Validation\Validator as v;

class MenuFornitoreController extends Controller
{
  public function getMenu($req, $res, $args)
  {
    $idFornitore = $args['id'];
    $data = [
      'fornitore'   => Utente::where('id', $idFornitore)->firstOrFail(),
      'categorie'   => CategoriaIngrediente::all(),
      'queryParams' => $req->getQueryParams(),
      'menu'        => $this->getMenuFornitore($idFornitore, $req),
    ];
    return $this->view->render($res, 'cliente/menuFornitore.twig', $data);
  }

  private function getMenuFornitore($idFornitore, $req)
  {
    $listini = Menu::where('id_fornitore', $idFornitore)->where('attivo', '=', '1')->get();

    // Non ho trovato un modo con la query...
    foreach ($listini as $l) {
      $piatti = [];
      $qParams = $req->getQueryParams();
      if (\sizeof($qParams) && !empty($qParams['filter'])) {
        $qCategoria = $qParams['filter'];
        $categoriaIngrediente = CategoriaIngrediente::where('nome', 'like', "%$qCategoria%")->firstOrFail();
        foreach ($l->piatti as $piatto) {
          $accept = true;
          foreach ($piatto->ingredienti as $ingrediente) {
            if ($ingrediente->id_categoria != $categoriaIngrediente->id) {
              $accept = false;
              break;
            }
          }
          if ($accept) {
            \array_push($piatti, $piatto);
          }
        }
      } else {
        $piatti = $l->piatti;
      }
      $l->setAttribute('piatti', $piatti);
    }
    
    return $listini;
  }
}