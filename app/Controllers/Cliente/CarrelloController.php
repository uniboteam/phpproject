<?php

namespace WebPranzo\Controllers\Cliente;
use WebPranzo\Models\TipoPagamento;
use WebPranzo\Models\TipoSpedizione;
use WebPranzo\Models\StatoSpedizione;
use WebPranzo\Models\Citta;
use WebPranzo\Models\luogoConsegna;
use WebPranzo\Models\Ordine;
use WebPranzo\Models\Piatto;
use WebPranzo\Models\Ingrediente;
use WebPranzo\Models\DettagliOrdine;
use WebPranzo\Models\DettagliPagamento;
use WebPranzo\Models\DettagliSpedizione;
use WebPranzo\Models\Utente;
use WebPranzo\Controllers\Controller;
use Respect\Validation\Validator as v;

class CarrelloController extends Controller
{
  public function getCarrello($req, $res, $args)
  {
    if (empty($_SESSION['carrello'])) {
      return $res->withRedirect($this->router->pathFor('cliente.ricerca.risultato'));
    }
    
    $data = [
      'tipiPagamento'  => TipoPagamento::all(),
      'luoghiConsegna' => LuogoConsegna::all(),
      'tipiSpedizione' => TipoSpedizione::all(),
    ];
    return $this->view->render($res, 'cliente/carrello.twig', $data);
  }

  public function postCarrello($req, $res)
  {
    if (empty($req->getParam('piatti'))) {
      return $res->withRedirect($this->router->pathFor('cliente.ricerca.risultato'));
    }

    $_SESSION['carrello'] = [
      'idFornitore' => $req->getParam('idFornitore'),
      'idCliente'   => $_SESSION['user'],
      'sconto'      => 0,
      'totale'      => 0,
      'piatti'      => [],
    ];

    $piatti = $req->getParam('piatti');
    foreach ($piatti as $key => $value) {
      $_SESSION['carrello']['piatti'][$value] = [
        'quantita' => $req->getParam('piatto_' . $value . '_quantita'),
        'ingredienti' => $req->getParam('piatto_' . $value),
      ];
    }
    return $res->withRedirect($this->router->pathFor('cliente.carrello'));
  }

  public function postCarrelloProcedi($req, $res)
  {
    $_SESSION['carrello']['datiSpedizione'] = [
      'indirizzo_id'    => $req->getParam('luogoConsegna'),
      'tipo_spedizione' => $req->getParam('tipo_spedizione'),
    ];

    $_SESSION['carrello']['tipoPagamento'] = $req->getParam('tipoPagamento');
    if (TipoPagamento::find($req->getParam('tipoPagamento'))->nome == 'Carta di credito') {
      return $res->withRedirect($this->router->pathFor('cliente.carrello.pagamento'));
    } else {
      return $res->withRedirect($this->router->pathFor('cliente.carrello.riepilogo'));
    }
  }

  public function getCarrelloPagamento($req, $res, $args)
  {
    if (empty($_SESSION['carrello'])) {
      return $res->withRedirect($this->router->pathFor('cliente.ricerca.risultato'));
    }

    $data = [
    ];
    return $this->view->render($res, 'cliente/carrelloPagamento.twig', $data);
  }

  public function postCarrelloPagamento($req, $res)
  {
    $validation = $this->validator->validate($req, [
      'carta'                => v::notEmpty(),
      'intestatario_nome'    => v::notEmpty(),
      'intestatario_cognome' => v::notEmpty(),
      'cvv'                  => v::noWhitespace()->notEmpty()->numeric(),
    ]);

    if ($validation->failed()) {
      return $res->withRedirect($this->router->pathFor('cliente.carrello.pagamento'));
    }

    $time = strtotime($req->getParam('mese') . '/'. '01'. '/'. $req->getParam('anno'));
    $newformat = date('Y-m-d', $time);

    $_SESSION['carrello']['datiPagamento'] = [
      'numero_carta'         => $req->getParam('carta'),
      'intestatario_nome'    => $req->getParam('intestatario_nome'),
      'intestatario_cognome' => $req->getParam('intestatario_cognome'),
      'cvv'                  => $req->getParam('cvv'),
      'data_scadenza'        => $newformat,
    ];

    return $res->withRedirect($this->router->pathFor('cliente.carrello.riepilogo'));
  }

  public function getCarrelloRiepilogo($req, $res, $args)
  {
    if (empty($_SESSION['carrello'])) {
      return $res->withRedirect($this->router->pathFor('cliente.ricerca.risultato'));
    }

    $tipoPagamento = TipoPagamento::find($_SESSION['carrello']['tipoPagamento']);
    $subtotale = 0;
    $sconto = Utente::find($_SESSION['carrello']['idFornitore'])->sconto->valore;
    $totale = 0;

    $piatti = [];
    foreach ($_SESSION['carrello']['piatti'] as $kPiatto => $vPiatto) {
      $p = Piatto::find($kPiatto);
      $subtotale += ($p->prezzo * $vPiatto['quantita']);
      $piatto = [
        'nome'        => $p->nome,
        'quantita'    => $vPiatto['quantita'],
        'ingredienti' => []
      ];
      foreach ($vPiatto['ingredienti'] as $key => $value) {
        $i = Ingrediente::find($value);
        \array_push($piatto['ingredienti'], [
          'nome' => $i->nome,
        ]);
      }
      \array_push($piatti, $piatto);
    }

    $tipoSpedizione = TipoSpedizione::find($_SESSION['carrello']['datiSpedizione']['tipo_spedizione']);
    $totale = $subtotale - ($subtotale * $sconto / 100) + $tipoSpedizione->tariffa;
    $_SESSION['carrello']['sconto'] = $sconto;
    $_SESSION['carrello']['totale'] = $totale;
    
    $data = [
      'piatti' => $piatti,
      'indirizzo_spedizione' => [
        'luogo' => LuogoConsegna::find($_SESSION['carrello']['datiSpedizione']['indirizzo_id'])->nome,
      ],
      'pagamento'  => $tipoPagamento->nome,
      'spedizione' => $tipoSpedizione,
      'subtotale'  => $subtotale,
      'sconto'     => $_SESSION['carrello']['sconto'],
      'totale'     => $_SESSION['carrello']['totale'],
    ];
    return $this->view->render($res, 'cliente/riepilogoOrdine.twig', $data);
  }

  public function postCarrelloOrdina($req, $res)
  {
    /**
     * Procedura di creazione ordine:
     * 1. Creo ordine con id fornitore e id cliente
     * 2. Creo tanti dettagli ordine quanti sono i piatti
     * 3. Creo dettagli pagamento
     * 4. Creo dettagli spedizione
     */
    $ordine = $this->createOrdine($_SESSION['carrello']);
    $idOrdine = $ordine->getKey();

    $this->createDettagliOrdine($idOrdine, $_SESSION['carrello']);
    $dettagliPagamento = $this->createDettagliPagamento($idOrdine, $_SESSION['carrello']);
    $dettagliSpedizione = $this->createDettagliSpedizione($idOrdine, $_SESSION['carrello']);

    unset($_SESSION['carrello']);
    
    $this->flash->addMessage('info', 'Il tuo ordine e stato completato con successo');
    return $res->withRedirect($this->router->pathFor('cliente.ricerca'));
  }

  private function createOrdine($carrello)
  {
    return Ordine::create([
      'sconto'       => $carrello['sconto'],
      'totale'       => $carrello['totale'],
      'evaso'        => '0',
      'id_fornitore' => $carrello['idFornitore'],
      'id_cliente'   => $carrello['idCliente'],
    ]);
  }

  private function createDettagliOrdine($idOrdine, $carrello)
  {
    foreach ($carrello['piatti'] as $key => $value) {
      DettagliOrdine::create([
        'id_ordine' => $idOrdine,
        'id_piatto' => $key,
        'quantita'  => $value['quantita'],
      ]);
    }
  }

  private function createDettagliPagamento($idOrdine, $carrello)
  {
    $res = (object)[];
    
    $tipoPagamento = TipoPagamento::find($carrello['tipoPagamento']);
    switch ($tipoPagamento->nome) {
      case 'Carta di credito':
        $res = DettagliPagamento::create([
          'id_ordine'            => $idOrdine,
          'contanti'             => '0',
          'numero_carta'         => $carrello['datiPagamento']['numero_carta'],
          'intestatario_nome'    => $carrello['datiPagamento']['intestatario_nome'],
          'intestatario_cognome' => $carrello['datiPagamento']['intestatario_cognome'],
          'data_scadenza'        => $carrello['datiPagamento']['data_scadenza'],
          'cvv'                  => $carrello['datiPagamento']['cvv'],
          'id_tipo_pagamento'    => $carrello['tipoPagamento'],
        ]);
        break;
      case 'Contanti alla consegna':
        $res = DettagliPagamento::create([
          'id_ordine'         => $idOrdine,
          'contanti'          => '1',
          'id_tipo_pagamento' => $carrello['tipoPagamento'],
        ]);
        break;
      default:
        break;
    }
    return $res;
  }

  private function createDettagliSpedizione($idOrdine, $carrello)
  {
    $statoSpedizione = StatoSpedizione::where('nome', '=', 'In elaborazione')->firstOrFail();

    return DettagliSpedizione::create([
      'indirizzo_id'       => $carrello['datiSpedizione']['indirizzo_id'],
      'consegnata'         => '0',
      'id_tipo_spedizione' => $carrello['datiSpedizione']['tipo_spedizione'],
      'id_stato'           => $statoSpedizione->id,
      'id_fattorino'       => $carrello['idFornitore'],
      'id_ordine'          => $idOrdine,
    ]);
  }
}