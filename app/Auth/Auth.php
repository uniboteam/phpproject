<?php

namespace WebPranzo\Auth;
use WebPranzo\Models\Utente;
use WebPranzo\Models\TipoUtente;

class Auth
{
  public function user()
  {
    if($this->check()){
      return Utente::find($_SESSION['user']);
    }
  }

  private function getTipoUtente($user)
  {
    return TipoUtente::where('id', $user->tipo)->firstOrFail();
  }

  public function isCliente()
  {
    $user = $this->user();
    if (empty($user)) {
      return false;
    }
    $tipoUtente = $this->getTipoUtente($user);
    return $tipoUtente->nome == 'Cliente';
  }

  public function isFornitore()
  {
    $user = $this->user();
    if (empty($user)) {
      return false;
    }
    $tipoUtente = $this->getTipoUtente($user);
    return $tipoUtente->nome == 'Fornitore';
  }

  public function isAdmin()
  {
    $user = $this->user();
    if (empty($user)) {
      return false;
    }
    $tipoUtente = $this->getTipoUtente($user);
    return $tipoUtente->nome == 'Amministratore';
  }

  public function isFattorino()
  {
    $user = $this->user();
    if (empty($user)) {
      return false;
    }
    $tipoUtente = $this->getTipoUtente($user);
    return $tipoUtente->nome == 'Fattorino';
  }

  public function getUserCredentials()
  {
    $user = $this->user();
    if ($user) {
      switch (TipoUtente::where('id', $user->tipo)->firstOrFail()->nome) {
        case 'Cliente':
          return $user->nome . ' ' . $user->cognome;
          break;
        case 'Fornitore':
          return $user->nome_attivita;
          break;
        case 'Amministratore':
          return $user->nome . ' ' . $user->cognome;
          break;
        case 'Fattorino':
          return 'Fattorino';
          break;
        default:
          return 'Nome non trovato';
          break;
      }
    }

  }

  public function check()
  {
    return isset($_SESSION['user']);
  }

  public function attempt($email, $password)
  {
    $user = Utente::where('email', $email)->first();
    if (!$user) {
      return false;
    }
    
    if (\password_verify($password, $user->password)) {
      $_SESSION['user'] = $user->id;
      return true;
    }

    return false;
  }
  
  public function logOut()
  {
    // unset($_SESSION['user']);
    session_unset();
  }
}