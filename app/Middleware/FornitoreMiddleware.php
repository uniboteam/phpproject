<?php

namespace WebPranzo\Middleware;

class FornitoreMiddleware extends Middleware
{
  public function __invoke($req, $res, $next)
  {
    if (!$this->container->auth->isFornitore()) {
      $this->container->flash->addMessage('error', 'La pagina richiesta e\' riservata solo per i fornitori.');
      return $res->withRedirect($this->container->router->pathFor('home'));
    }
    
    $res = $next($req, $res);
    return $res;
  }
}