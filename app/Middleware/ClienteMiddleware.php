<?php

namespace WebPranzo\Middleware;

class ClienteMiddleware extends Middleware
{
  public function __invoke($req, $res, $next)
  {
    if (!$this->container->auth->isCliente()) {
      $this->container->flash->addMessage('error', 'La pagina richiesta e\' riservata solo per i clienti.');
      return $res->withRedirect($this->container->router->pathFor('home'));
    }
    
    $res = $next($req, $res);
    return $res;
  }
}