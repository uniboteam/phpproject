<?php

namespace WebPranzo\Middleware;

class AuthMiddleware extends Middleware
{
  public function __invoke($req, $res, $next)
  {
    if (!$this->container->auth->check()) {
      $this->container->flash->addMessage('error', 'Si prega di loggarsi');
      return $res->withRedirect($this->container->router->pathFor('auth.login'));
    }
    
    $res = $next($req, $res);
    return $res;
  }
}