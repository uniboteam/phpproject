<?php

namespace WebPranzo\Middleware;

class FattorinoMiddleware extends Middleware
{
  public function __invoke($req, $res, $next)
  {
    if (!$this->container->auth->isFattorino()) {
      $this->container->flash->addMessage('error', 'La pagina richiesta e\' riservata solo per i fattorini.');
      return $res->withRedirect($this->container->router->pathFor('home'));
    }
    
    $res = $next($req, $res);
    return $res;
  }
}