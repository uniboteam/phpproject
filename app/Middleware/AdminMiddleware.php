<?php

namespace WebPranzo\Middleware;

class AdminMiddleware extends Middleware
{
  public function __invoke($req, $res, $next)
  {
    if (!$this->container->auth->isAdmin()) {
      $this->container->flash->addMessage('error', 'La pagina richiesta e\' riservata solo per gli amministratori.');
      return $res->withRedirect($this->container->router->pathFor('home'));
    }
    
    $res = $next($req, $res);
    return $res;
  }
}