<?php

namespace WebPranzo\Models;

class Ingrediente extends BaseModel
{
  protected $table = 'ingrediente';

  protected $fillable = ['nome', 'descrizione', 'id_categoria'];

  public function categoria()
  {
    return $this->belongsTo('WebPranzo\Models\CategoriaIngrediente', 'id_categoria');
  }

  public function piatti()
  {
    return $this->belongsToMany('WebPranzo\Models\Piatto', 'dettagli_piatto', 'id_ingrediente', 'id_piatto');
  }
}