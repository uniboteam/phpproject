<?php

namespace WebPranzo\Models;

class Feedback extends BaseModel
{
  protected $table = 'feedback';

  protected $fillable = [
    'descrizione_id',
    'id_ordine',
    'data_creazione',
    'voto',
    'id_cliente',
    'id_piatto',
  ];

  public function ordine()
  {
    return $this->belongsTo('WebPranzo\Models\Ordine', 'id_ordine');
  }

  public function cliente()
  {
    return $this->belongsTo('WebPranzo\Models\Utente', 'id_cliente');
  }

  public function piatto()
  {
    return $this->belongsTo('WebPranzo\Models\Piatto', 'id_piatto');
  }

  public function descrizione()
  {
    return $this->hasOne('WebPranzo\Models\TestoFeedback', 'id', 'descrizione_id');
  }

}