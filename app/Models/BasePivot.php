<?php

namespace WebPranzo\Models;
use Illuminate\Database\Eloquent\Relations\Pivot;

class BasePivot extends Pivot
{
  public $timestamps = false;
}