<?php

namespace WebPranzo\Models;

class DettagliPagamento extends BasePivot
{
  protected $table = 'dettagli_pagamento';

  protected $fillable = [
    'id_ordine',
    'contanti',
    'numero_carta',
    'intestatario_nome',
    'intestatario_cognome',
    'intestatario_nome_attivita',
    'data_scadenza',
    'cvv',
    'id_tipo_pagamento',
  ];

  public function ordine()
  {
    return $this->belongsTo('WebPranzo\Models\Ordine', 'id_ordine');
  }
  
  public function tipoPagamento()
  {
    return $this->belongsTo('WebPranzo\Models\TipoPagamento', 'id_tipo_pagamento');
  }

}