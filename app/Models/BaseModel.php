<?php

namespace WebPranzo\Models;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
  public $timestamps = false;
}