<?php

namespace WebPranzo\Models;

class Utente extends BaseModel
{
  protected $table = 'utente';

  protected $fillable = [
    'nome',
    'cognome',
    'nome_attivita',
    'iva',
    'punti_vantaggio',
    'indirizzo_via',
    'indirizzo_numero',
    'indirizzo_citta',
    'email',
    'password',
    'telefono_fisso',
    'telefono_cellulare',
    'logo',
    'tipo',
    'id_specializzazione',
    'orario_id',
    'id_sconto',
  ];

  public function setPassword($password)
  {
    $this->update([
      'password' => password_hash($password, PASSWORD_DEFAULT)
    ]);
  }

  public function citta()
  {
    return $this->belongsTo('WebPranzo\Models\Citta', 'indirizzo_citta');
  }

  public function tipoUtente()
  {
    return $this->belongsTo('WebPranzo\Models\TipoUtente', 'tipo');
  }

  public function orarioSettimanale()
  {
    return $this->belongsTo('WebPranzo\Models\OrarioSettimanale', 'orario_id');
  }

  public function ordini()
  {
    return $this->hasMany('WebPranzo\Models\Ordine', 'id_fornitore');
  }

  public function sconto()
  {
    return $this->belongsTo('WebPranzo\Models\Sconto', 'id_sconto');
  }
}