<?php

namespace WebPranzo\Models;

class Piatto extends BaseModel
{
  protected $table = 'piatto';

  protected $fillable = ['nome', 'descrizione', 'prezzo', 'peso', 'id_tipo_cucina'];

  public function tipoCucina()
  {
    return $this->belongsTo('WebPranzo\Models\TipoCucina', 'id_tipo_cucina');
  }

  public function menu()
  {
    return $this->belongsToMany('WebPranzo\Models\Menu', 'dettagli_listino', 'id_piatto', 'id_listino');
  }

  public function ingredienti()
  {
    return $this->belongsToMany('WebPranzo\Models\Ingrediente', 'dettagli_piatto', 'id_piatto', 'id_ingrediente');
  }
}