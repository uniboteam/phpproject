<?php

namespace WebPranzo\Models;

class TipoUtente extends BaseModel
{
  protected $table = 'tipo_utente';

  protected $fillable = ['nome', 'descrizione'];

  public function utente()
  {
    return $this->hasMany('WebPranzo\Models\Utente', 'tipo');
  }
}