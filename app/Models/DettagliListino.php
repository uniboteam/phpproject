<?php

namespace WebPranzo\Models;

class DettagliListino extends BasePivot
{
  protected $table = 'dettagli_listino';

  protected $fillable = ['id_piatto', 'id_listino'];

  public function listino()
  {
    return $this->belongsTo('WebPranzo\Models\Menu', 'id_listino');
  }

  public function piatto()
  {
    return $this->belongsTo('WebPranzo\Models\Piatto', 'id_piatto');
  }
}