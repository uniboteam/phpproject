<?php

namespace WebPranzo\Models;

class TestoFeedback extends BaseModel
{
  protected $table = 'testo_feedback';

  protected $fillable = [
    'testo',
  ];
}