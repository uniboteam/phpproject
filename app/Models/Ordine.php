<?php

namespace WebPranzo\Models;

class Ordine extends BaseModel
{
  protected $table = 'ordine';

  protected $fillable = ['sconto', 'totale', 'evaso', 'id_fornitore', 'id_cliente'];

  public function piatti()
  {
    return $this->belongsToMany('WebPranzo\Models\Piatto', 'dettagli_ordine', 'id_ordine', 'id_piatto')->withPivot('quantita');
  }

  public function cliente()
  {
    return $this->belongsTo('WebPranzo\Models\Utente', 'id_cliente');
  }

  public function fornitore()
  {
    return $this->belongsTo('WebPranzo\Models\Utente', 'id_fornitore');
  }

  public function dettagliPagamento()
  {
    return $this->hasOne('WebPranzo\Models\DettagliPagamento', 'id_ordine');
  }

  public function dettagliSpedizione()
  {
    return $this->hasOne('WebPranzo\Models\DettagliSpedizione', 'id_ordine');
  }

  public function feedback()
  {
    return $this->hasMany('WebPranzo\Models\Feedback', 'id_ordine');
  }

  public function distinctFeedback()
  {
    return $this->feedback()->groupBy('id_ordine');
  }
}