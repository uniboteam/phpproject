<?php

namespace WebPranzo\Models;

class DettagliOrdine extends BasePivot
{
  protected $table = 'dettagli_ordine';

  protected $fillable = ['id_piatto', 'id_ordine', 'quantita'];

  public function piatto()
  {
    return $this->belongsTo('WebPranzo\Models\Piatto', 'id_piatto');
  }

  public function ordine()
  {
    return $this->belongsTo('WebPranzo\Models\Ordine', 'id_ordine');
  }
}