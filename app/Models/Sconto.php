<?php

namespace WebPranzo\Models;

class Sconto extends BaseModel
{
  protected $table = 'sconto';

  protected $fillable = ['nome', 'valore'];
}