<?php

namespace WebPranzo\Models;

class CategoriaIngrediente extends BaseModel
{
  protected $table = 'categoria_ingrediente';

  protected $fillable = ['nome'];
}