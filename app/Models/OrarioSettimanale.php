<?php

namespace WebPranzo\Models;

class OrarioSettimanale extends BaseModel
{
  protected $table = 'orario_settimanale';

  protected $fillable = [
    'lunedi_dal',
    'lunedi_al',
    'martedi_dal',
    'martedi_al',
    'mercoledi_dal',
    'mercoledi_al',
    'giovedi_dal',
    'giovedi_al',
    'venerdi_dal',
    'venerdi_al',
    'sabato_dal',
    'sabato_al',
    'domenica_dal',
    'domenica_al',
  ];
}