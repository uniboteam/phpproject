<?php

namespace WebPranzo\Models;

class Notifica extends BaseModel
{
  protected $table = 'notifica';

  protected $fillable = [
    'data_creazione',
    'data_lettura',
    'titolo',
    'testo',
    'id_fornitore',
    'id_cliente',
    'id_ordine',
  ];

  public function ordine()
  {
    return $this->belongsTo('WebPranzo\Models\Ordine', 'id_ordine');
  }

  public function cliente()
  {
    return $this->belongsTo('WebPranzo\Models\Utente', 'id_cliente');
  }

  public function fornitore()
  {
    return $this->belongsTo('WebPranzo\Models\Utente', 'id_fornitore');
  }
}