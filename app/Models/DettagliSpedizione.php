<?php

namespace WebPranzo\Models;

class DettagliSpedizione extends BasePivot
{
  protected $table = 'dettagli_spedizione';

  protected $fillable = [
    'indirizzo_id',
    'data_arrivo',
    'consegnata',
    'id_tipo_spedizione',
    'id_stato',
    'id_fattorino',
    'id_ordine',
  ];

  public function tipoSpedizione()
  {
    return $this->belongsTo('WebPranzo\Models\TipoSpedizione', 'id_tipo_spedizione');
  }

  public function statoSpedizione()
  {
    return $this->belongsTo('WebPranzo\Models\StatoSpedizione', 'id_stato');
  }

  public function fattorino()
  {
    return $this->belongsTo('WebPranzo\Models\Utente', 'id_fattorino');
  }

  public function ordine()
  {
    return $this->belongsTo('WebPranzo\Models\Ordine', 'id_ordine');
  }

  public function luogoConsegna()
  {
    return $this->belongsTo('WebPranzo\Models\LuogoConsegna', 'indirizzo_id');
  }

}