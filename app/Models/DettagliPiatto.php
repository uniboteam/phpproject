<?php

namespace WebPranzo\Models;

class DettagliPiatto extends BasePivot
{
  protected $table = 'dettagli_piatto';

  protected $fillable = ['id_piatto', 'id_ingrediente', 'quantita'];

  public function piatto()
  {
    return $this->belongsTo('WebPranzo\Models\Piatto', 'id_piatto');
  }

  public function ingrediente()
  {
    return $this->belongsTo('WebPranzo\Models\Ingrediente', 'id_ingrediente');
  }
}