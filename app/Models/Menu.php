<?php

namespace WebPranzo\Models;

class Menu extends BaseModel
{
  protected $table = 'listino';

  protected $fillable = ['nome', 'attivo', 'id_fornitore'];

  public function fornitore()
  {
    return $this->belongsTo('WebPranzo\Models\Utente', 'id_fornitore');
  }

  public function piatti()
  {
    return $this->belongsToMany('WebPranzo\Models\Piatto', 'dettagli_listino', 'id_listino', 'id_piatto');
  }
}