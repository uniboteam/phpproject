-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 10.0.3              
-- * Generator datetime: Aug 17 2017              
-- * Generation datetime: Sun Jan  6 19:23:50 2019 
-- * LUN file: C:\Users\KopacbDesk\Documents\TW1819\docs\db_model\model.lun 
-- * Schema: logic/2 
-- ********************************************* 


-- Database Section
-- ________________ 

create database webpranzo;
use webpranzo;


-- Tables Section
-- _____________ 

create table Categoria_ingrediente (
     id int not null AUTO_INCREMENT,
     nome varchar(255) not null,
     constraint IDCategoria_ingrediente primary key (id));

create table Citta (
     id int not null AUTO_INCREMENT,
     nome varchar(255) not null,
     constraint IDCitta primary key (id));

create table Dettagli_listino (
     id_piatto int not null,
     id_listino int not null,
     constraint IDDettagli_listino primary key (id_piatto, id_listino));

create table Dettagli_ordine (
     id_piatto int not null,
     id_ordine int not null,
     quantita bigint not null,
     constraint IDDettagli_ordine primary key (id_ordine, id_piatto));

create table Dettagli_pagamento (
     id int not null AUTO_INCREMENT,
     id_ordine int not null,
     contanti char not null,
     numero_carta varchar(255),
     intestatario_nome varchar(255),
     intestatario_cognome varchar(255),
     intestatario_nome_attivita varchar(255),
     data_scadenza datetime,
     cvv varchar(255),
     id_tipo_pagamento int not null,
     constraint IDDettagli_pagamento primary key (id),
     constraint FKPagare_ID unique (id_ordine));

create table Dettagli_piatto (
     id_piatto int not null,
     id_ingrediente int not null,
     quantita bigint not null,
     constraint IDDettagli_piatto primary key (id_piatto, id_ingrediente));

create table Dettagli_spedizione (
     id int not null AUTO_INCREMENT,
     data_arrivo datetime,
     consegnata char not null,
     id_tipo_spedizione int not null,
     id_stato int not null,
     id_fattorino int not null,
     id_ordine int not null,
     indirizzo_id int not null,
     constraint IDDettagli_spedizione primary key (id));

create table Feedback (
     id int not null AUTO_INCREMENT,
     descrizione_id int,
     id_ordine int not null,
     data_creazione datetime not null,
     voto int not null,
     id_cliente int not null,
     id_piatto int not null,
     constraint IDFeedback primary key (id));

create table Foto (
     id int not null AUTO_INCREMENT,
     percorso varchar(255) not null,
     id_piatto int not null,
     constraint IDFoto primary key (id));

create table Ingrediente (
     id int not null AUTO_INCREMENT,
     nome varchar(255) not null,
     descrizione varchar(255) not null,
     id_categoria int not null,
     constraint IDIngrediente primary key (id));

create table Listino (
     id int not null AUTO_INCREMENT,
     nome varchar(255) not null,
     attivo char not null,
     id_fornitore int not null,
     constraint IDListino primary key (id));

create table LuogoConsegna (
     id int not null AUTO_INCREMENT,
     nome varchar(255) not null,
     descrizione varchar(255) not null,
     constraint IDLuogoConsegna primary key (id));

create table Notifica (
     id int not null AUTO_INCREMENT,
     data_creazione datetime not null,
     data_lettura datetime not null,
     titolo varchar(255) not null,
     testo varchar(255) not null,
     id_fornitore int not null,
     id_cliente int not null,
     id_ordine int not null,
     constraint IDNotifica primary key (id));

create table Orario_settimanale (
     id int not null AUTO_INCREMENT,
     lunedi_dal varchar(255) not null,
     lunedi_al varchar(255) not null,
     martedi_dal varchar(255) not null,
     martedi_al varchar(255) not null,
     mercoledi_dal varchar(255) not null,
     mercoledi_al varchar(255) not null,
     giovedi_dal varchar(255) not null,
     giovedi_al varchar(255) not null,
     venerdi_dal varchar(255) not null,
     venerdi_al varchar(255) not null,
     sabato_dal varchar(255) not null,
     sabato_al varchar(255) not null,
     domenica_dal varchar(255) not null,
     domenica_al varchar(255) not null,
     constraint IDOrario_settimanale primary key (id));

create table Ordine (
     id int not null AUTO_INCREMENT,
     sconto float(10) not null,
     totale float(10) not null,
     evaso char not null,
     id_fornitore int not null,
     id_cliente int not null,
     constraint IDOrdine_ID primary key (id));

create table Piatto (
     id int not null AUTO_INCREMENT,
     nome varchar(255) not null,
     descrizione varchar(255) not null,
     prezzo float(10) not null,
     peso float(10) not null,
     id_tipo_cucina int not null,
     constraint IDPiatto_ID primary key (id));

create table Riserva_ingrediente (
     id_ingrediente int not null,
     id_fornitore int not null,
     quantita bigint not null,
     constraint IDRiserva_ingrediente primary key (id_fornitore, id_ingrediente));

create table Sconto (
     id int not null AUTO_INCREMENT,
     nome varchar(255) not null,
     valore int not null,
     constraint IDSconto primary key (id));

create table Stato_spedizione (
     id int not null AUTO_INCREMENT,
     nome varchar(255) not null,
     descrizione varchar(255) not null,
     constraint IDStato_spedizione primary key (id));

create table Testo_feedback (
     id int not null AUTO_INCREMENT,
     testo varchar(255) not null,
     constraint IDTesto_feedback_ID primary key (id));

create table Tipo_cucina (
     id int not null AUTO_INCREMENT,
     nome varchar(255) not null,
     constraint IDTipo_cucina primary key (id));

create table Tipo_pagamento (
     id int not null AUTO_INCREMENT,
     nome varchar(255) not null,
     tasso_transazione int not null,
     constraint IDTipo_pagamento primary key (id));

create table Tipo_spedizione (
     id int not null AUTO_INCREMENT,
     nome varchar(255) not null,
     tariffa float(10) not null,
     id_tipo_tariffa int not null,
     constraint IDTipo_spedizione primary key (id));

create table Tipo_tariffa (
     id int not null AUTO_INCREMENT,
     nome varchar(255) not null,
     constraint IDTipo_tariffa primary key (id));

create table Tipo_utente (
     id int not null AUTO_INCREMENT,
     nome varchar(255) not null,
     descrizione varchar(255) not null,
     constraint IDTipo_utente primary key (id));

create table Utente (
     id int not null AUTO_INCREMENT,
     nome varchar(255),
     cognome varchar(255),
     nome_attivita varchar(255),
     iva varchar(255),
     punti_vantaggio int,
     indirizzo_via varchar(255) not null,
     indirizzo_numero varchar(255) not null,
     email varchar(255) not null,
     password varchar(255) not null,
     telefono_fisso varchar(255),
     telefono_fax varchar(255),
     telefono_cellulare varchar(255),
     logo varchar(255),
     ultimo_accesso datetime not null,
     tipo int not null,
     id_specializzazione int,
     indirizzo_citta int not null,
     orario_id int,
     id_sconto int,
     constraint IDUtente primary key (id));


-- Constraints Section
-- ___________________ 

alter table Dettagli_listino add constraint FKCon_Lis
     foreign key (id_listino)
     references Listino (id);

alter table Dettagli_listino add constraint FKCon_Pia
     foreign key (id_piatto)
     references Piatto (id);

alter table Dettagli_ordine add constraint FKCom_Ord
     foreign key (id_ordine)
     references Ordine (id);

alter table Dettagli_ordine add constraint FKCom_Pia
     foreign key (id_piatto)
     references Piatto (id);

alter table Dettagli_pagamento add constraint FKRiferire1
     foreign key (id_tipo_pagamento)
     references Tipo_pagamento (id);

alter table Dettagli_pagamento add constraint FKPagare_FK
     foreign key (id_ordine)
     references Ordine (id);

alter table Dettagli_piatto add constraint FKInc_Ing
     foreign key (id_ingrediente)
     references Ingrediente (id);

alter table Dettagli_piatto add constraint FKInc_Pia
     foreign key (id_piatto)
     references Piatto (id);

alter table Dettagli_spedizione add constraint FKRiferire
     foreign key (id_tipo_spedizione)
     references Tipo_spedizione (id);

alter table Dettagli_spedizione add constraint FKPossedere
     foreign key (id_stato)
     references Stato_spedizione (id);

alter table Dettagli_spedizione add constraint FKAggiornare1
     foreign key (id_fattorino)
     references Utente (id);

alter table Dettagli_spedizione add constraint FKSpedizione
     foreign key (id_ordine)
     references Ordine (id);

alter table Dettagli_spedizione add constraint FKConsegnare_1
     foreign key (indirizzo_id)
     references LuogoConsegna (id);

alter table Feedback add constraint FKAssegnare
     foreign key (id_cliente)
     references Utente (id);

alter table Feedback add constraint FKRicevere
     foreign key (id_piatto)
     references Piatto (id);

alter table Feedback add constraint FKDescrizione_FK
     foreign key (descrizione_id)
     references Testo_feedback (id);

alter table Feedback add constraint FKValutazione_FK
     foreign key (id_ordine)
     references Ordine (id);

alter table Foto add constraint FKContenere
     foreign key (id_piatto)
     references Piatto (id);

alter table Ingrediente add constraint FKAppartenere1
     foreign key (id_categoria)
     references Categoria_ingrediente (id);

alter table Listino add constraint FKAggiornare
     foreign key (id_fornitore)
     references Utente (id);

alter table Notifica add constraint FKMandare
     foreign key (id_fornitore)
     references Utente (id);

alter table Notifica add constraint FKLeggere
     foreign key (id_cliente)
     references Utente (id);

alter table Notifica add constraint FKCollegarsi
     foreign key (id_ordine)
     references Ordine (id);

-- Not implemented
-- alter table Ordine add constraint IDOrdine_CHK
--     check(exists(select * from Dettagli_pagamento
--                  where Dettagli_pagamento.id_ordine = id)); 

-- Not implemented
-- alter table Ordine add constraint IDOrdine_CHK
--     check(exists(select * from Dettagli_spedizione
--                  where Dettagli_spedizione.id_ordine = id)); 

-- Not implemented
-- alter table Ordine add constraint IDOrdine_CHK
--     check(exists(select * from Dettagli_ordine
--                  where Dettagli_ordine.id_ordine = id)); 

alter table Ordine add constraint FKEvadere
     foreign key (id_fornitore)
     references Utente (id);

alter table Ordine add constraint FKEseguire
     foreign key (id_cliente)
     references Utente (id);

-- Not implemented
-- alter table Piatto add constraint IDPiatto_CHK
--     check(exists(select * from Dettagli_piatto
--                  where Dettagli_piatto.id_piatto = id)); 

alter table Piatto add constraint FKAppartenere
     foreign key (id_tipo_cucina)
     references Tipo_cucina (id);

alter table Riserva_ingrediente add constraint FKGestire
     foreign key (id_fornitore)
     references Utente (id);

alter table Riserva_ingrediente add constraint FKGestire1
     foreign key (id_ingrediente)
     references Ingrediente (id);

-- Not implemented
-- alter table Testo_feedback add constraint IDTesto_feedback_CHK
--     check(exists(select * from Feedback
--                  where Feedback.descrizione_id = id)); 

alter table Tipo_spedizione add constraint FKRiferirsi 
     foreign key (id_tipo_tariffa)
     references Tipo_tariffa (id);

alter table Utente add constraint FKEssere
     foreign key (tipo)
     references Tipo_utente (id);

alter table Utente add constraint FKSpecializzarsi
     foreign key (id_specializzazione)
     references Tipo_cucina (id);

alter table Utente add constraint FKRisiedere
     foreign key (indirizzo_citta)
     references Citta (id);

alter table Utente add constraint FKLavorare
     foreign key (orario_id)
     references Orario_settimanale (id);

alter table Utente add constraint FKOffrire
     foreign key (id_sconto)
     references Sconto (id);


-- Index Section
-- _____________ 

