-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2019 at 10:18 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webpranzo`
--
CREATE DATABASE IF NOT EXISTS `webpranzo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `webpranzo`;

-- --------------------------------------------------------

--
-- Table structure for table `categoria_ingrediente`
--

CREATE TABLE `categoria_ingrediente` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categoria_ingrediente`
--

INSERT INTO `categoria_ingrediente` (`id`, `nome`) VALUES
(1, 'Vegetariano'),
(2, 'Vegano'),
(3, 'Ciliaco'),
(4, 'Carne'),
(5, 'Pesce');

-- --------------------------------------------------------

--
-- Table structure for table `citta`
--

CREATE TABLE `citta` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `citta`
--

INSERT INTO `citta` (`id`, `nome`) VALUES
(1, 'Cesena'),
(2, 'Forli'),
(3, 'Bologna'),
(4, 'Faenza');

-- --------------------------------------------------------

--
-- Table structure for table `dettagli_listino`
--

CREATE TABLE `dettagli_listino` (
  `id_piatto` int(11) NOT NULL,
  `id_listino` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dettagli_listino`
--

INSERT INTO `dettagli_listino` (`id_piatto`, `id_listino`) VALUES
(1, 1),
(2, 1),
(3, 1),
(5, 3),
(6, 4),
(7, 5),
(8, 5),
(9, 5),
(10, 5),
(11, 5),
(12, 6),
(13, 1),
(14, 4),
(15, 3),
(16, 7),
(17, 7),
(18, 7),
(19, 7),
(20, 7),
(21, 8),
(22, 8),
(23, 9),
(24, 9),
(25, 10),
(26, 10),
(27, 10),
(28, 11),
(29, 11),
(30, 12),
(31, 13),
(32, 2),
(33, 2),
(34, 14),
(35, 14);

-- --------------------------------------------------------

--
-- Table structure for table `dettagli_ordine`
--

CREATE TABLE `dettagli_ordine` (
  `id_piatto` int(11) NOT NULL,
  `id_ordine` int(11) NOT NULL,
  `quantita` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dettagli_ordine`
--

INSERT INTO `dettagli_ordine` (`id_piatto`, `id_ordine`, `quantita`) VALUES
(1, 18, 1),
(2, 18, 2),
(3, 18, 3),
(5, 18, 4),
(6, 18, 5),
(1, 19, 1),
(5, 20, 2),
(2, 21, 1),
(1, 22, 1),
(2, 23, 3),
(6, 24, 1),
(3, 25, 1),
(5, 25, 1),
(15, 25, 1),
(2, 26, 1),
(3, 26, 1),
(14, 26, 1),
(15, 26, 1),
(9, 27, 4),
(21, 28, 2),
(24, 28, 1),
(33, 29, 3),
(25, 30, 1),
(27, 30, 1),
(29, 30, 2),
(3, 31, 2),
(5, 31, 1),
(14, 31, 1),
(15, 31, 1),
(7, 32, 1),
(9, 32, 6),
(19, 33, 1),
(11, 34, 2),
(15, 35, 3);

-- --------------------------------------------------------

--
-- Table structure for table `dettagli_pagamento`
--

CREATE TABLE `dettagli_pagamento` (
  `id` int(11) NOT NULL,
  `id_ordine` int(11) NOT NULL,
  `contanti` char(1) NOT NULL,
  `numero_carta` varchar(255) DEFAULT NULL,
  `intestatario_nome` varchar(255) DEFAULT NULL,
  `intestatario_cognome` varchar(255) DEFAULT NULL,
  `intestatario_nome_attivita` varchar(255) DEFAULT NULL,
  `data_scadenza` datetime DEFAULT NULL,
  `cvv` varchar(255) DEFAULT NULL,
  `id_tipo_pagamento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dettagli_pagamento`
--

INSERT INTO `dettagli_pagamento` (`id`, `id_ordine`, `contanti`, `numero_carta`, `intestatario_nome`, `intestatario_cognome`, `intestatario_nome_attivita`, `data_scadenza`, `cvv`, `id_tipo_pagamento`) VALUES
(16, 18, '1', NULL, NULL, NULL, NULL, NULL, NULL, 2),
(17, 19, '1', NULL, NULL, NULL, NULL, NULL, NULL, 2),
(18, 20, '0', '1111111111111111111', 'Marioooo', 'Super', NULL, '2019-01-01 00:00:00', '11123', 1),
(19, 21, '1', NULL, NULL, NULL, NULL, NULL, NULL, 2),
(20, 22, '0', '0002369874569874', 'Mario', 'Rossi', NULL, '2019-01-01 00:00:00', '213', 1),
(21, 23, '0', '8000 7000 9000 0001', 'Sophia', 'Fantoni', NULL, '2019-01-01 00:00:00', '999', 1),
(22, 24, '0', '0000000000000000000', 'm', 'r', NULL, '2019-01-01 00:00:00', '111', 1),
(23, 25, '1', NULL, NULL, NULL, NULL, NULL, NULL, 2),
(24, 26, '0', '4000 2256 8000 125', 'Mario', 'Rossi', NULL, '2019-01-01 00:00:00', '122', 1),
(25, 27, '1', NULL, NULL, NULL, NULL, NULL, NULL, 2),
(26, 28, '0', '8000 1122 3598 6655', 'Michele', 'Prati', NULL, '2019-01-01 00:00:00', '560', 1),
(27, 29, '1', NULL, NULL, NULL, NULL, NULL, NULL, 2),
(28, 30, '0', '8888 5555 6963 2145', 'Luca', 'Bosi', NULL, '2019-01-01 00:00:00', '002', 1),
(29, 31, '1', NULL, NULL, NULL, NULL, NULL, NULL, 2),
(30, 32, '0', '8987 5696 6300 0001', 'Claudia', 'Valeriani', NULL, '2019-01-01 00:00:00', '007', 1),
(31, 33, '1', NULL, NULL, NULL, NULL, NULL, NULL, 2),
(32, 34, '1', NULL, NULL, NULL, NULL, NULL, NULL, 2),
(33, 35, '1', NULL, NULL, NULL, NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `dettagli_piatto`
--

CREATE TABLE `dettagli_piatto` (
  `id_piatto` int(11) NOT NULL,
  `id_ingrediente` int(11) NOT NULL,
  `quantita` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dettagli_piatto`
--

INSERT INTO `dettagli_piatto` (`id_piatto`, `id_ingrediente`, `quantita`) VALUES
(1, 1, 10),
(1, 2, 20),
(1, 3, 30),
(2, 1, 10),
(2, 3, 30),
(2, 10, 20),
(3, 3, 30),
(3, 11, 20),
(3, 19, 10),
(5, 5, 0),
(6, 6, 0),
(6, 7, 0),
(6, 8, 0),
(7, 9, 0),
(7, 10, 0),
(7, 11, 0),
(7, 12, 0),
(8, 13, 0),
(8, 14, 0),
(8, 15, 0),
(8, 16, 0),
(9, 17, 0),
(9, 18, 0),
(9, 19, 0),
(9, 20, 0),
(10, 21, 0),
(10, 22, 0),
(11, 23, 0),
(11, 24, 0),
(11, 25, 0),
(12, 26, 0),
(12, 27, 0),
(12, 28, 0),
(12, 29, 0),
(13, 2, 0),
(13, 7, 0),
(13, 12, 0),
(13, 30, 0),
(14, 1, 0),
(14, 31, 0),
(15, 32, 0),
(16, 34, 0),
(16, 35, 0),
(17, 37, 0),
(18, 34, 0),
(18, 35, 0),
(18, 36, 0),
(19, 38, 0),
(20, 35, 0),
(20, 37, 0),
(20, 39, 0),
(21, 40, 0),
(22, 41, 0),
(22, 42, 0),
(22, 43, 0),
(23, 44, 0),
(24, 45, 0),
(25, 46, 0),
(26, 47, 0),
(27, 48, 0),
(27, 49, 0),
(27, 50, 0),
(28, 51, 0),
(28, 52, 0),
(28, 53, 0),
(29, 54, 0),
(30, 55, 0),
(31, 56, 0),
(31, 57, 0),
(32, 39, 0),
(32, 58, 0),
(33, 59, 0),
(34, 5, 0),
(34, 39, 0),
(34, 49, 0),
(35, 32, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dettagli_spedizione`
--

CREATE TABLE `dettagli_spedizione` (
  `id` int(11) NOT NULL,
  `data_arrivo` datetime DEFAULT NULL,
  `consegnata` char(1) NOT NULL,
  `id_tipo_spedizione` int(11) NOT NULL,
  `id_stato` int(11) NOT NULL,
  `id_fattorino` int(11) NOT NULL,
  `id_ordine` int(11) NOT NULL,
  `indirizzo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dettagli_spedizione`
--

INSERT INTO `dettagli_spedizione` (`id`, `data_arrivo`, `consegnata`, `id_tipo_spedizione`, `id_stato`, `id_fattorino`, `id_ordine`, `indirizzo_id`) VALUES
(6, '2019-01-06 21:45:20', '1', 3, 4, 4, 18, 4),
(7, NULL, '0', 1, 2, 4, 19, 4),
(8, NULL, '0', 1, 1, 4, 20, 23),
(9, NULL, '0', 1, 1, 4, 21, 4),
(10, NULL, '0', 1, 1, 4, 22, 2),
(11, '2019-01-07 08:37:41', '1', 1, 4, 4, 23, 12),
(12, NULL, '0', 1, 1, 4, 24, 2),
(13, NULL, '0', 1, 1, 4, 25, 4),
(14, NULL, '0', 2, 1, 4, 26, 44),
(15, NULL, '0', 3, 1, 24, 27, 1),
(16, NULL, '0', 1, 2, 22, 28, 32),
(17, NULL, '0', 3, 1, 6, 29, 2),
(18, NULL, '0', 2, 1, 23, 30, 45),
(19, NULL, '0', 1, 1, 4, 31, 3),
(20, NULL, '0', 1, 1, 24, 32, 20),
(21, NULL, '0', 1, 1, 22, 33, 5),
(22, NULL, '0', 1, 1, 24, 34, 1),
(23, NULL, '0', 1, 1, 4, 35, 3);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `descrizione_id` int(11) DEFAULT NULL,
  `id_ordine` int(11) NOT NULL,
  `data_creazione` datetime NOT NULL,
  `voto` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_piatto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `descrizione_id`, `id_ordine`, `data_creazione`, `voto`, `id_cliente`, `id_piatto`) VALUES
(1, 1, 18, '2019-01-06 12:11:38', 1, 5, 1),
(2, 1, 18, '2019-01-06 12:11:38', 2, 5, 2),
(3, 1, 18, '2019-01-06 12:11:38', 3, 5, 3),
(4, 1, 18, '2019-01-06 12:11:38', 4, 5, 5),
(5, 1, 18, '2019-01-06 12:11:38', 5, 5, 6),
(6, 2, 19, '2019-01-06 13:05:21', 5, 5, 1),
(7, 3, 21, '2019-01-07 20:46:13', 4, 5, 2),
(8, 4, 28, '2019-01-07 21:05:39', 5, 26, 21),
(9, 4, 28, '2019-01-07 21:05:39', 5, 26, 24);

-- --------------------------------------------------------

--
-- Table structure for table `foto`
--

CREATE TABLE `foto` (
  `id` int(11) NOT NULL,
  `percorso` varchar(255) NOT NULL,
  `id_piatto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ingrediente`
--

CREATE TABLE `ingrediente` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descrizione` varchar(255) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingrediente`
--

INSERT INTO `ingrediente` (`id`, `nome`, `descrizione`, `id_categoria`) VALUES
(1, 'Pollo', 'Buonissima Passata al pomodoro', 4),
(2, 'Insalata', 'Buonissimo Uovo di gallina', 2),
(3, 'Radicchio', 'Buonissimo Latte', 1),
(4, 'Mega ingrediente', '', 2),
(5, 'Mozarelline', '', 1),
(6, 'Carne', '', 1),
(7, 'Aglio', '', 1),
(8, 'Peppe', '', 3),
(9, 'Pomodoro', '', 2),
(10, 'Mozzarella', '', 1),
(11, 'Pane', '', 1),
(12, 'Ceddar', '', 1),
(13, 'Cotoletta', '', 4),
(14, 'Insalata', '', 2),
(15, 'Pane', '', 1),
(16, 'Maionese', '', 1),
(17, 'Speck', '', 4),
(18, 'Caprino', '', 1),
(19, 'Bacon', '', 4),
(20, 'Pate di olive', '', 2),
(21, 'Speck', '', 4),
(22, 'Brie', '', 1),
(23, 'Prosciutto cotto', '', 4),
(24, 'Mozzarella', '', 1),
(25, 'Carote', '', 3),
(26, 'Prosciutto cotto', '', 4),
(27, 'Fontina', '', 1),
(28, 'Tonno', '', 5),
(29, 'Pane', '', 1),
(30, 'Svizzera', '', 4),
(31, 'Peperoncino', '', 2),
(32, 'Patate', '', 2),
(33, 'Anguilla', '', 5),
(34, 'Passata di pomodoro', '', 1),
(35, 'Mozzarella', '', 1),
(36, 'Funghi', '', 1),
(37, 'Passata di pomodoro', '', 2),
(38, 'Wurstel', '', 4),
(39, 'Patate fritte', '', 2),
(40, 'Ingredienti a scelta', '', 3),
(41, 'Prosciutto cotto', '', 4),
(42, 'Cipolla', '', 2),
(43, 'Bufala', '', 1),
(44, 'Mascarpone', '', 3),
(45, 'Nutella', '', 3),
(46, 'Ragu', '', 4),
(47, 'Salmone', '', 5),
(48, 'Pomodoro', '', 2),
(49, 'Olive', '', 2),
(50, 'Origano', '', 2),
(51, 'Salsiccia', '', 4),
(52, 'Salame', '', 4),
(53, 'Pancetta', '', 4),
(54, 'Pollo', '', 4),
(55, 'Patate', '', 3),
(56, 'Panna', '', 1),
(57, 'Frutti di bosco', '', 1),
(58, 'Hamburger', '', 4),
(59, 'Tofu', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `listino`
--

CREATE TABLE `listino` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `attivo` char(1) NOT NULL,
  `id_fornitore` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `listino`
--

INSERT INTO `listino` (`id`, `nome`, `attivo`, `id_fornitore`) VALUES
(1, 'Menu del McDonalds', '1', 4),
(2, 'Menu del giorno', '1', 6),
(3, 'Sfizione', '1', 4),
(4, 'Piccante', '1', 4),
(5, 'Panini settimanali', '1', 24),
(6, 'Panini weekend', '0', 24),
(7, 'Pizze classiche', '1', 22),
(8, 'Pizze Speciali', '1', 22),
(9, 'Pizze dolci', '1', 22),
(10, 'Primi', '1', 23),
(11, 'Secondi', '1', 23),
(12, 'Contorni', '1', 23),
(13, 'Dolci', '0', 23),
(14, 'Antipasti', '1', 6);

-- --------------------------------------------------------

--
-- Table structure for table `luogoconsegna`
--

CREATE TABLE `luogoconsegna` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descrizione` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `luogoconsegna`
--

INSERT INTO `luogoconsegna` (`id`, `nome`, `descrizione`) VALUES
(1, 'Ingresso piano terra', ''),
(2, 'Aula Magna 3.4', ''),
(3, 'Spazio Studenti', ''),
(4, 'Aula 2.1', ''),
(5, 'Lab VeLa 2.2', ''),
(6, 'Aula 2.3', ''),
(7, 'Aula 2.4', ''),
(8, 'Aula 2.5', ''),
(9, 'Aula 2.6', ''),
(10, 'Aula 2.7', ''),
(11, 'Aula 2.8', ''),
(12, 'Aula 2.9', ''),
(13, 'Aula 2.10', ''),
(14, 'Aula 2.11', ''),
(15, 'Aula 2.12', ''),
(16, 'Aula 2.13', ''),
(17, 'Laboratorio LaMo', ''),
(18, 'Laboratorio Disegno 2.17', ''),
(19, 'Laboratorio Disegno 2.16', ''),
(20, 'Laboratorio Disegno 2.15', ''),
(21, 'Laboratorio Disegno 2.14', ''),
(22, 'Laboratorio LIB', ''),
(23, 'Laboratorio LIB2', ''),
(24, 'Laboratorio informatico 3.1', ''),
(25, 'Laboratorio LeLe 3.2', ''),
(26, 'Laboratorio', 'Piano primo'),
(27, 'Laboratorio 1 Elettr e Telec', 'Piano primo'),
(28, 'Laboratorio ICM', 'Piano primo'),
(29, 'Laboratorio informatico 3.3', 'Piano primo'),
(30, 'Laboratorio disegno 3.5', 'Piano primo'),
(31, 'Laboratorio disegno 3.6', 'Piano primo'),
(32, 'Aula 3.7', 'Piano primo'),
(33, 'Laboratorio disegno 3.8', 'Piano primo'),
(34, 'Laboratorio disegno 3.9', 'Piano primo'),
(35, 'Laboratorio LaMo', 'Piano primo'),
(36, 'Laboratorio La Mo VidA', 'Piano primo'),
(37, 'Aula 3.10', 'Piano primo'),
(38, 'Aula 3.11', 'Piano primo'),
(39, 'Laboratorio di sisntesi 3.12', 'Piano primo'),
(40, 'Laboratorio di sisntesi 3.13', 'Piano primo'),
(41, 'Laboratorio di sisntesi 3.14', 'Piano primo'),
(42, 'Laboratorio di sintesi 3.15', 'Piano primo'),
(43, 'Laboratorio di sintesi 3.16', 'Piano primo'),
(44, 'Zona ristoro', 'Piano primo'),
(45, 'Ingresso piano primo', 'Piano primo');

-- --------------------------------------------------------

--
-- Table structure for table `notifica`
--

CREATE TABLE `notifica` (
  `id` int(11) NOT NULL,
  `data_creazione` datetime NOT NULL,
  `data_lettura` datetime NOT NULL,
  `titolo` varchar(255) NOT NULL,
  `testo` varchar(255) NOT NULL,
  `id_fornitore` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_ordine` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifica`
--

INSERT INTO `notifica` (`id`, `data_creazione`, `data_lettura`, `titolo`, `testo`, `id_fornitore`, `id_cliente`, `id_ordine`) VALUES
(1, '2019-01-05 13:44:58', '0000-00-00 00:00:00', 'Ordine evaso', 'Il tuo ordine e stato evaso e a breve sara consegnato da uno dei nostri speditori. Grazie per tuo ordine!', 4, 5, 18),
(2, '2019-01-06 13:05:01', '0000-00-00 00:00:00', 'Ordine evaso', 'Gentile cliente, il suo ordine è stato evaso dal fornitore. A breve le sarà consegnato presso il luogo da lei prescelto. Speriamo di aver soddisfatto le sue richieste e attendiamo il suo prossimo ordine. Buona gionata!!!', 4, 5, 19),
(3, '2019-01-06 15:44:58', '0000-00-00 00:00:00', 'Ordine evaso', 'Gentile cliente, il suo ordine è stato evaso dal fornitore. A breve le sarà consegnato presso il luogo da lei prescelto. Speriamo di aver soddisfatto le sue richieste e attendiamo il suo prossimo ordine. Buona gionata!!!', 4, 5, 20),
(4, '2019-01-06 18:55:25', '0000-00-00 00:00:00', 'Ordine evaso', 'Gentile cliente, il suo ordine è stato evaso dal fornitore. A breve le sarà consegnato presso il luogo da lei prescelto. Speriamo di aver soddisfatto le sue richieste e attendiamo il suo prossimo ordine. Buona gionata!!!', 4, 5, 18),
(5, '2019-01-06 20:25:19', '0000-00-00 00:00:00', 'Ordine evaso', 'Gentile cliente, il suo ordine è stato evaso dal fornitore. A breve le sarà consegnato presso il luogo da lei prescelto. Speriamo di aver soddisfatto le sue richieste e attendiamo il suo prossimo ordine. Buona gionata!!!', 4, 5, 21),
(6, '2019-01-06 21:39:30', '0000-00-00 00:00:00', 'Ordine è spedito', 'Gentile cliente, il suo ordine è spedito.', 14, 5, 18),
(7, '2019-01-06 21:43:20', '0000-00-00 00:00:00', 'Ordine è in arrivo', 'Gentile cliente, il suo ordine è in oggi.', 14, 5, 18),
(8, '2019-01-06 21:45:20', '0000-00-00 00:00:00', 'Ordine è arrivato', 'Gentile cliente, il suo ordine è stato arrivato.', 14, 5, 18),
(9, '2019-01-06 21:46:08', '0000-00-00 00:00:00', 'Ordine è spedito', 'Gentile cliente, il suo ordine è spedito.', 14, 5, 19),
(10, '2019-01-07 08:36:27', '0000-00-00 00:00:00', 'Ordine evaso', 'Gentile cliente, il suo ordine è stato evaso dal fornitore. A breve le sarà consegnato presso il luogo da lei prescelto. Speriamo di aver soddisfatto le sue richieste e attendiamo il suo prossimo ordine. Buona gionata!!!', 4, 17, 23),
(11, '2019-01-07 08:37:17', '0000-00-00 00:00:00', 'Ordine è in arrivo', 'Gentile cliente, il suo ordine è in arrivo oggi.', 14, 17, 23),
(12, '2019-01-07 08:37:41', '0000-00-00 00:00:00', 'Ordine è arrivato', 'Gentile cliente, il suo ordine è stato arrivato.', 14, 17, 23),
(13, '2019-01-07 20:57:00', '0000-00-00 00:00:00', 'Ordine evaso', 'Gentile cliente, il suo ordine è stato evaso dal fornitore. A breve le sarà consegnato presso il luogo da lei prescelto. Speriamo di aver soddisfatto le sue richieste e attendiamo il suo prossimo ordine. Buona gionata!!!', 22, 26, 28),
(14, '2019-01-07 21:11:07', '0000-00-00 00:00:00', 'Ordine è spedito', 'Gentile cliente, il suo ordine è spedito.', 14, 26, 28);

-- --------------------------------------------------------

--
-- Table structure for table `orario_settimanale`
--

CREATE TABLE `orario_settimanale` (
  `id` int(11) NOT NULL,
  `lunedi_dal` varchar(255) NOT NULL,
  `lunedi_al` varchar(255) NOT NULL,
  `martedi_dal` varchar(255) NOT NULL,
  `martedi_al` varchar(255) NOT NULL,
  `mercoledi_dal` varchar(255) NOT NULL,
  `mercoledi_al` varchar(255) NOT NULL,
  `giovedi_dal` varchar(255) NOT NULL,
  `giovedi_al` varchar(255) NOT NULL,
  `venerdi_dal` varchar(255) NOT NULL,
  `venerdi_al` varchar(255) NOT NULL,
  `sabato_dal` varchar(255) NOT NULL,
  `sabato_al` varchar(255) NOT NULL,
  `domenica_dal` varchar(255) NOT NULL,
  `domenica_al` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orario_settimanale`
--

INSERT INTO `orario_settimanale` (`id`, `lunedi_dal`, `lunedi_al`, `martedi_dal`, `martedi_al`, `mercoledi_dal`, `mercoledi_al`, `giovedi_dal`, `giovedi_al`, `venerdi_dal`, `venerdi_al`, `sabato_dal`, `sabato_al`, `domenica_dal`, `domenica_al`) VALUES
(5, '9.45', '22.30', '----', '----', '6.00', '23.00', '11.00', '22.00', '9.00', '24.00', '6.00', '24.00', '11.00', '22.00'),
(6, '11.00', '15.00', '12.00', '20.00', '11.00', '15.00', '--------', '---------', '10.30', '22.00', '11.00', '15.00', '12.00', '22.00'),
(7, '10.00', '16.00', '11.00', '20.00', '11.30', '20.30', '----', '----', '11.30', '20.30', '11.30', '23.00', '11.30', '15.30');

-- --------------------------------------------------------

--
-- Table structure for table `ordine`
--

CREATE TABLE `ordine` (
  `id` int(11) NOT NULL,
  `sconto` float NOT NULL,
  `totale` float NOT NULL,
  `evaso` char(1) NOT NULL,
  `id_fornitore` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordine`
--

INSERT INTO `ordine` (`id`, `sconto`, `totale`, `evaso`, `id_fornitore`, `id_cliente`) VALUES
(18, 0, 1159, '1', 4, 5),
(19, 0, 20, '1', 4, 5),
(20, 0, 201, '1', 4, 5),
(21, 10, 23, '1', 4, 5),
(22, 10, 18.5, '0', 4, 5),
(23, 10, 59, '1', 4, 17),
(24, 10, 115.7, '0', 4, 5),
(25, 10, 15.35, '0', 4, 25),
(26, 10, 23.37, '0', 4, 5),
(27, 0, 25, '0', 24, 5),
(28, 0, 33.9, '1', 22, 26),
(29, 0, 31, '0', 6, 26),
(30, 5, 34.975, '0', 23, 26),
(31, 10, 26.6, '0', 4, 25),
(32, 0, 37.5, '0', 24, 25),
(33, 0, 11.5, '0', 22, 25),
(34, 0, 14, '0', 24, 25),
(35, 10, 10.4, '0', 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `piatto`
--

CREATE TABLE `piatto` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descrizione` varchar(255) NOT NULL,
  `prezzo` float NOT NULL,
  `peso` float NOT NULL,
  `id_tipo_cucina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `piatto`
--

INSERT INTO `piatto` (`id`, `nome`, `descrizione`, `prezzo`, `peso`, `id_tipo_cucina`) VALUES
(1, 'Insalata basic', 'Pizza completamente senza senso', 4.3, 1, 1),
(2, 'Insalata cesar', 'Pizza completamente senza senso', 4.8, 1, 1),
(3, 'Crispy', 'Pizza completamente senza senso', 7.5, 1, 1),
(5, 'Mozzarelline', '', 2, 0, 1),
(6, 'Burritto', '', 13.2, 0, 1),
(7, 'Caprese', '', 5.5, 0, 1),
(8, 'Cotoletta', '', 7.3, 0, 1),
(9, 'My Way', '', 4.5, 0, 1),
(10, 'Tirolese', '', 3.5, 0, 1),
(11, 'Special', '', 4.5, 0, 1),
(12, 'Gustoso', '', 6.5, 0, 1),
(13, '1955', '', 8.5, 0, 1),
(14, 'Alette di pollo', '', 5, 0, 1),
(15, 'Patatine fritte', '', 2, 0, 1),
(16, 'Margherita', '', 4.5, 0, 1),
(17, 'Marinara', '', 3.5, 0, 1),
(18, 'Funghi', '', 5, 0, 1),
(19, 'Wurstel', '', 6.5, 0, 1),
(20, 'Patate fritte', '', 6.5, 0, 1),
(21, 'Fantasia del pizzaiolo', '', 11.2, 0, 1),
(22, '4 sapori', '', 8.5, 0, 1),
(23, 'Mascarpone', '', 6, 0, 1),
(24, 'Nutella', '', 6.5, 0, 1),
(25, 'Cappelletti al ragu', '', 8, 0, 1),
(26, 'Tagliolini al salmone', '', 10, 0, 1),
(27, 'Penne all\'arrabbiata', '', 5.5, 0, 1),
(28, 'Grigliata', '', 18, 0, 1),
(29, 'Petto di pollo alla griglia', '', 8.5, 0, 1),
(30, 'Patate al forno', '', 3.5, 0, 1),
(31, 'Panna cotta', '', 5.3, 0, 1),
(32, 'Panino e patate fritte', '', 10, 0, 1),
(33, 'Hamburger vegano', '', 8, 0, 1),
(34, 'Antipasto misto', '', 7, 0, 1),
(35, 'Tortillas', '', 3.5, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `riserva_ingrediente`
--

CREATE TABLE `riserva_ingrediente` (
  `id_ingrediente` int(11) NOT NULL,
  `id_fornitore` int(11) NOT NULL,
  `quantita` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sconto`
--

CREATE TABLE `sconto` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `valore` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sconto`
--

INSERT INTO `sconto` (`id`, `nome`, `valore`) VALUES
(1, 'Sconto 1', 0),
(2, 'Sconto 2', 5),
(3, 'Sconto 3', 10),
(4, 'Sconto 4', 15),
(5, 'Sconto 5', 20),
(6, 'Sconto 6', 25),
(7, 'Sconto 7', 30);

-- --------------------------------------------------------

--
-- Table structure for table `stato_spedizione`
--

CREATE TABLE `stato_spedizione` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descrizione` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stato_spedizione`
--

INSERT INTO `stato_spedizione` (`id`, `nome`, `descrizione`) VALUES
(1, 'In elaborazione', 'La consegna ancora non spedita'),
(2, 'Spedito', 'La consegna spedita'),
(3, 'In arrivo', 'La consegna in arrivo'),
(4, 'Arrivata', 'La consegna arrivata');

-- --------------------------------------------------------

--
-- Table structure for table `testo_feedback`
--

CREATE TABLE `testo_feedback` (
  `id` int(11) NOT NULL,
  `testo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testo_feedback`
--

INSERT INTO `testo_feedback` (`id`, `testo`) VALUES
(1, 'Bellisima esperienza'),
(2, 'Meraviglioso'),
(3, 'Ingredienti freschi e golosi, il fattorino è stato molto gentile, l\'unica considerazione che non mi permette di concedere 5 stelle è che il pane non era croccante come avrei voluto. Riprovero\' sicuramente!!!'),
(4, 'Il pizzaiolo ha proprio indovinato i miei gusti! Pizza veramente speciale. Pizza con la nutella molto buona e giustissima per partire bene con le lezioni pomeridiane! Consigliato ');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_cucina`
--

CREATE TABLE `tipo_cucina` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_cucina`
--

INSERT INTO `tipo_cucina` (`id`, `nome`) VALUES
(1, 'Romagnola'),
(2, 'Street food'),
(3, 'Pizzeria'),
(4, 'Gurmet'),
(5, 'Trattoria'),
(6, 'Osteria'),
(7, 'Agriturismo'),
(8, 'Pesce'),
(9, 'Mensa'),
(10, 'Paninoteca');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_pagamento`
--

CREATE TABLE `tipo_pagamento` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `tasso_transazione` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_pagamento`
--

INSERT INTO `tipo_pagamento` (`id`, `nome`, `tasso_transazione`) VALUES
(1, 'Carta di credito', 0),
(2, 'Contanti alla consegna', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tipo_spedizione`
--

CREATE TABLE `tipo_spedizione` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `tariffa` float NOT NULL,
  `id_tipo_tariffa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_spedizione`
--

INSERT INTO `tipo_spedizione` (`id`, `nome`, `tariffa`, `id_tipo_tariffa`) VALUES
(1, 'Ordinaria', 5, 1),
(2, 'Veloce', 6, 1),
(3, 'Urgente', 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tipo_tariffa`
--

CREATE TABLE `tipo_tariffa` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_tariffa`
--

INSERT INTO `tipo_tariffa` (`id`, `nome`) VALUES
(1, 'Tariffa ordinaria'),
(2, 'Tariffa con supplemento');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_utente`
--

CREATE TABLE `tipo_utente` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descrizione` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_utente`
--

INSERT INTO `tipo_utente` (`id`, `nome`, `descrizione`) VALUES
(1, 'Amministratore', 'taglia le dita a tutti'),
(2, 'Cliente', 'colui che ha una fame feroce'),
(3, 'Fornitore', 'pregate che vi dia del cibo'),
(4, 'Fattorino', 'io consegno con una velocita super sonica');

-- --------------------------------------------------------

--
-- Table structure for table `utente`
--

CREATE TABLE `utente` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `cognome` varchar(255) DEFAULT NULL,
  `nome_attivita` varchar(255) DEFAULT NULL,
  `iva` varchar(255) DEFAULT NULL,
  `punti_vantaggio` int(11) DEFAULT NULL,
  `indirizzo_via` varchar(255) NOT NULL,
  `indirizzo_numero` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `telefono_fisso` varchar(255) DEFAULT NULL,
  `telefono_fax` varchar(255) DEFAULT NULL,
  `telefono_cellulare` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `ultimo_accesso` datetime NOT NULL,
  `tipo` int(11) NOT NULL,
  `id_specializzazione` int(11) DEFAULT NULL,
  `indirizzo_citta` int(11) NOT NULL,
  `orario_id` int(11) DEFAULT NULL,
  `id_sconto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utente`
--

INSERT INTO `utente` (`id`, `nome`, `cognome`, `nome_attivita`, `iva`, `punti_vantaggio`, `indirizzo_via`, `indirizzo_numero`, `email`, `password`, `telefono_fisso`, `telefono_fax`, `telefono_cellulare`, `logo`, `ultimo_accesso`, `tipo`, `id_specializzazione`, `indirizzo_citta`, `orario_id`, `id_sconto`) VALUES
(4, '', '', 'McDonalds', NULL, NULL, 'Via Emilia Ponente', '864', 'fornitore@mail.com', '$2y$10$eiLN3yXvQjnGkXXdahGFBeoFJa/AuBpcBzwR556xbOYmd2QNVJIL2', NULL, NULL, '+39 0543 822956', 'http://www.partagenatal.com.br/media/mcdonalds-300x231.jpg', '0000-00-00 00:00:00', 3, 2, 1, 5, 3),
(5, 'Mario', 'Rossi', '', NULL, NULL, '', '', 'cliente@mail.com', '$2y$10$eiLN3yXvQjnGkXXdahGFBeoFJa/AuBpcBzwR556xbOYmd2QNVJIL2', NULL, NULL, '+39 333 1234 5566', '', '0000-00-00 00:00:00', 2, 1, 1, NULL, NULL),
(6, NULL, NULL, 'America Graffiti', NULL, NULL, 'Piazza A. Moro', '170', 'fornitore2@mail.com', '$2y$10$GffWAyhYzLL20PF7fpNuk.uPomZz4S2KOAprDBfBnNzjj3F5pHOhm', NULL, NULL, '+39 0543 222597', 'https://www.confimprese.it/wp-content/uploads/2018/05/America-Graffiti_CMYK_2017-lbox-555x400-ffffff.png', '0000-00-00 00:00:00', 3, 2, 1, NULL, 1),
(14, NULL, NULL, NULL, NULL, NULL, '', '', 'fattorino@mail.com', '$2y$10$Fpwgg1LRrYjdU1PbusFo8es3na.V8eDKQz2cIjl71couNjaEf33HG', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', 4, NULL, 1, NULL, NULL),
(17, 'Mario', 'Bianchi', NULL, NULL, NULL, '', '', 'admin@mail.com', '$2y$10$eiLN3yXvQjnGkXXdahGFBeoFJa/AuBpcBzwR556xbOYmd2QNVJIL2', NULL, NULL, '3333225698', NULL, '0000-00-00 00:00:00', 1, NULL, 1, NULL, NULL),
(22, NULL, NULL, 'Languorino', NULL, NULL, 'V. Angeli', '43', 'fornitore3@mail.com', '$2y$10$rkB2LYcuhoO.nUGo56dLs.8JcNh7bEE5hCozIkUNwa3zgAF6EayXC', NULL, NULL, '0543 559863', 'http://www.premiocambiamenti.it/wp-content/uploads/2017/07/Logo.jpg', '0000-00-00 00:00:00', 3, 3, 1, 7, 1),
(23, NULL, NULL, 'Margo\' Cesena', NULL, NULL, 'Via G. Bovio', '294', 'fornitore4@mail.com', '$2y$10$o7MxYC.73fFaZrETnr5FJu4kG.Rhr1KPNn.CN9RdRfj/i3tSsVDeK', NULL, NULL, '0543 556988', 'http://paceboca.wpengine.com/wp-content/uploads/2016/12/Margo-Logo_2c-300x200.jpg', '0000-00-00 00:00:00', 3, 1, 1, NULL, 2),
(24, NULL, NULL, 'Ifood', NULL, NULL, 'M. Angeloni', '335', 'fornitore5@mail.com', '$2y$10$TXVkL06zV3UDQ0dPhVK1HuMP4bSckxyxjNM7GYQ0yS8zZgW6K3oq6', NULL, NULL, '0543882210', 'https://dcassetcdn.com/design_img/1442230/51618/51618_7323624_1442230_c91dc92a_image.jpg', '0000-00-00 00:00:00', 3, 10, 1, 6, 1),
(25, 'Claudia', 'Verdi', NULL, NULL, NULL, '', '', 'cliente2@mail.com', '$2y$10$.GTNTsCVFA3CnSOsHnaiJuiLYWD6AIQ3iVKbh3aO9pBS5PQWIhtzC', NULL, NULL, '3357755001', NULL, '0000-00-00 00:00:00', 2, NULL, 1, NULL, NULL),
(26, 'Luca', 'Bosi', NULL, NULL, NULL, '', '', 'cliente3@mail.com', '$2y$10$eYDOq2Kfo06irBej4oaMRuL3Q1eiuq477kjqO3eCbdSqNRe8w6lI.', NULL, NULL, '3489962103', NULL, '0000-00-00 00:00:00', 2, NULL, 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria_ingrediente`
--
ALTER TABLE `categoria_ingrediente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `citta`
--
ALTER TABLE `citta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dettagli_listino`
--
ALTER TABLE `dettagli_listino`
  ADD PRIMARY KEY (`id_piatto`,`id_listino`),
  ADD KEY `FKCon_Lis` (`id_listino`);

--
-- Indexes for table `dettagli_ordine`
--
ALTER TABLE `dettagli_ordine`
  ADD PRIMARY KEY (`id_ordine`,`id_piatto`),
  ADD KEY `FKCom_Pia` (`id_piatto`);

--
-- Indexes for table `dettagli_pagamento`
--
ALTER TABLE `dettagli_pagamento`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `FKPagare_ID` (`id_ordine`),
  ADD KEY `FKRiferire1` (`id_tipo_pagamento`);

--
-- Indexes for table `dettagli_piatto`
--
ALTER TABLE `dettagli_piatto`
  ADD PRIMARY KEY (`id_piatto`,`id_ingrediente`),
  ADD KEY `FKInc_Ing` (`id_ingrediente`);

--
-- Indexes for table `dettagli_spedizione`
--
ALTER TABLE `dettagli_spedizione`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKRiferire` (`id_tipo_spedizione`),
  ADD KEY `FKPossedere` (`id_stato`),
  ADD KEY `FKAggiornare1` (`id_fattorino`),
  ADD KEY `FKSpedizione` (`id_ordine`),
  ADD KEY `FKConsegnare_1` (`indirizzo_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKAssegnare` (`id_cliente`),
  ADD KEY `FKRicevere` (`id_piatto`),
  ADD KEY `FKDescrizione_FK` (`descrizione_id`),
  ADD KEY `FKValutazione_FK` (`id_ordine`);

--
-- Indexes for table `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKContenere` (`id_piatto`);

--
-- Indexes for table `ingrediente`
--
ALTER TABLE `ingrediente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKAppartenere1` (`id_categoria`);

--
-- Indexes for table `listino`
--
ALTER TABLE `listino`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKAggiornare` (`id_fornitore`);

--
-- Indexes for table `luogoconsegna`
--
ALTER TABLE `luogoconsegna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifica`
--
ALTER TABLE `notifica`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKMandare` (`id_fornitore`),
  ADD KEY `FKLeggere` (`id_cliente`),
  ADD KEY `FKCollegarsi` (`id_ordine`);

--
-- Indexes for table `orario_settimanale`
--
ALTER TABLE `orario_settimanale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordine`
--
ALTER TABLE `ordine`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKEvadere` (`id_fornitore`),
  ADD KEY `FKEseguire` (`id_cliente`);

--
-- Indexes for table `piatto`
--
ALTER TABLE `piatto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKAppartenere` (`id_tipo_cucina`);

--
-- Indexes for table `riserva_ingrediente`
--
ALTER TABLE `riserva_ingrediente`
  ADD PRIMARY KEY (`id_fornitore`,`id_ingrediente`),
  ADD KEY `FKGestire1` (`id_ingrediente`);

--
-- Indexes for table `sconto`
--
ALTER TABLE `sconto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stato_spedizione`
--
ALTER TABLE `stato_spedizione`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testo_feedback`
--
ALTER TABLE `testo_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipo_cucina`
--
ALTER TABLE `tipo_cucina`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipo_pagamento`
--
ALTER TABLE `tipo_pagamento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipo_spedizione`
--
ALTER TABLE `tipo_spedizione`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKRiferirsi` (`id_tipo_tariffa`);

--
-- Indexes for table `tipo_tariffa`
--
ALTER TABLE `tipo_tariffa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipo_utente`
--
ALTER TABLE `tipo_utente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKEssere` (`tipo`),
  ADD KEY `FKSpecializzarsi` (`id_specializzazione`),
  ADD KEY `FKRisiedere` (`indirizzo_citta`),
  ADD KEY `FKLavorare` (`orario_id`),
  ADD KEY `FKOffrire` (`id_sconto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria_ingrediente`
--
ALTER TABLE `categoria_ingrediente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `citta`
--
ALTER TABLE `citta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dettagli_pagamento`
--
ALTER TABLE `dettagli_pagamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `dettagli_spedizione`
--
ALTER TABLE `dettagli_spedizione`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `foto`
--
ALTER TABLE `foto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ingrediente`
--
ALTER TABLE `ingrediente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `listino`
--
ALTER TABLE `listino`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `luogoconsegna`
--
ALTER TABLE `luogoconsegna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `notifica`
--
ALTER TABLE `notifica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `orario_settimanale`
--
ALTER TABLE `orario_settimanale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ordine`
--
ALTER TABLE `ordine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `piatto`
--
ALTER TABLE `piatto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `sconto`
--
ALTER TABLE `sconto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stato_spedizione`
--
ALTER TABLE `stato_spedizione`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `testo_feedback`
--
ALTER TABLE `testo_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tipo_cucina`
--
ALTER TABLE `tipo_cucina`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tipo_pagamento`
--
ALTER TABLE `tipo_pagamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tipo_spedizione`
--
ALTER TABLE `tipo_spedizione`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tipo_tariffa`
--
ALTER TABLE `tipo_tariffa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tipo_utente`
--
ALTER TABLE `tipo_utente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `utente`
--
ALTER TABLE `utente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dettagli_listino`
--
ALTER TABLE `dettagli_listino`
  ADD CONSTRAINT `FKCon_Lis` FOREIGN KEY (`id_listino`) REFERENCES `listino` (`id`),
  ADD CONSTRAINT `FKCon_Pia` FOREIGN KEY (`id_piatto`) REFERENCES `piatto` (`id`);

--
-- Constraints for table `dettagli_ordine`
--
ALTER TABLE `dettagli_ordine`
  ADD CONSTRAINT `FKCom_Ord` FOREIGN KEY (`id_ordine`) REFERENCES `ordine` (`id`),
  ADD CONSTRAINT `FKCom_Pia` FOREIGN KEY (`id_piatto`) REFERENCES `piatto` (`id`);

--
-- Constraints for table `dettagli_pagamento`
--
ALTER TABLE `dettagli_pagamento`
  ADD CONSTRAINT `FKPagare_FK` FOREIGN KEY (`id_ordine`) REFERENCES `ordine` (`id`),
  ADD CONSTRAINT `FKRiferire1` FOREIGN KEY (`id_tipo_pagamento`) REFERENCES `tipo_pagamento` (`id`);

--
-- Constraints for table `dettagli_piatto`
--
ALTER TABLE `dettagli_piatto`
  ADD CONSTRAINT `FKInc_Ing` FOREIGN KEY (`id_ingrediente`) REFERENCES `ingrediente` (`id`),
  ADD CONSTRAINT `FKInc_Pia` FOREIGN KEY (`id_piatto`) REFERENCES `piatto` (`id`);

--
-- Constraints for table `dettagli_spedizione`
--
ALTER TABLE `dettagli_spedizione`
  ADD CONSTRAINT `FKAggiornare1` FOREIGN KEY (`id_fattorino`) REFERENCES `utente` (`id`),
  ADD CONSTRAINT `FKConsegnare_1` FOREIGN KEY (`indirizzo_id`) REFERENCES `luogoconsegna` (`id`),
  ADD CONSTRAINT `FKPossedere` FOREIGN KEY (`id_stato`) REFERENCES `stato_spedizione` (`id`),
  ADD CONSTRAINT `FKRiferire` FOREIGN KEY (`id_tipo_spedizione`) REFERENCES `tipo_spedizione` (`id`),
  ADD CONSTRAINT `FKSpedizione` FOREIGN KEY (`id_ordine`) REFERENCES `ordine` (`id`);

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `FKAssegnare` FOREIGN KEY (`id_cliente`) REFERENCES `utente` (`id`),
  ADD CONSTRAINT `FKDescrizione_FK` FOREIGN KEY (`descrizione_id`) REFERENCES `testo_feedback` (`id`),
  ADD CONSTRAINT `FKRicevere` FOREIGN KEY (`id_piatto`) REFERENCES `piatto` (`id`),
  ADD CONSTRAINT `FKValutazione_FK` FOREIGN KEY (`id_ordine`) REFERENCES `ordine` (`id`);

--
-- Constraints for table `foto`
--
ALTER TABLE `foto`
  ADD CONSTRAINT `FKContenere` FOREIGN KEY (`id_piatto`) REFERENCES `piatto` (`id`);

--
-- Constraints for table `ingrediente`
--
ALTER TABLE `ingrediente`
  ADD CONSTRAINT `FKAppartenere1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria_ingrediente` (`id`);

--
-- Constraints for table `listino`
--
ALTER TABLE `listino`
  ADD CONSTRAINT `FKAggiornare` FOREIGN KEY (`id_fornitore`) REFERENCES `utente` (`id`);

--
-- Constraints for table `notifica`
--
ALTER TABLE `notifica`
  ADD CONSTRAINT `FKCollegarsi` FOREIGN KEY (`id_ordine`) REFERENCES `ordine` (`id`),
  ADD CONSTRAINT `FKLeggere` FOREIGN KEY (`id_cliente`) REFERENCES `utente` (`id`),
  ADD CONSTRAINT `FKMandare` FOREIGN KEY (`id_fornitore`) REFERENCES `utente` (`id`);

--
-- Constraints for table `ordine`
--
ALTER TABLE `ordine`
  ADD CONSTRAINT `FKEseguire` FOREIGN KEY (`id_cliente`) REFERENCES `utente` (`id`),
  ADD CONSTRAINT `FKEvadere` FOREIGN KEY (`id_fornitore`) REFERENCES `utente` (`id`);

--
-- Constraints for table `piatto`
--
ALTER TABLE `piatto`
  ADD CONSTRAINT `FKAppartenere` FOREIGN KEY (`id_tipo_cucina`) REFERENCES `tipo_cucina` (`id`);

--
-- Constraints for table `riserva_ingrediente`
--
ALTER TABLE `riserva_ingrediente`
  ADD CONSTRAINT `FKGestire` FOREIGN KEY (`id_fornitore`) REFERENCES `utente` (`id`),
  ADD CONSTRAINT `FKGestire1` FOREIGN KEY (`id_ingrediente`) REFERENCES `ingrediente` (`id`);

--
-- Constraints for table `tipo_spedizione`
--
ALTER TABLE `tipo_spedizione`
  ADD CONSTRAINT `FKRiferirsi` FOREIGN KEY (`id_tipo_tariffa`) REFERENCES `tipo_tariffa` (`id`);

--
-- Constraints for table `utente`
--
ALTER TABLE `utente`
  ADD CONSTRAINT `FKEssere` FOREIGN KEY (`tipo`) REFERENCES `tipo_utente` (`id`),
  ADD CONSTRAINT `FKLavorare` FOREIGN KEY (`orario_id`) REFERENCES `orario_settimanale` (`id`),
  ADD CONSTRAINT `FKOffrire` FOREIGN KEY (`id_sconto`) REFERENCES `sconto` (`id`),
  ADD CONSTRAINT `FKRisiedere` FOREIGN KEY (`indirizzo_citta`) REFERENCES `citta` (`id`),
  ADD CONSTRAINT `FKSpecializzarsi` FOREIGN KEY (`id_specializzazione`) REFERENCES `tipo_cucina` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
