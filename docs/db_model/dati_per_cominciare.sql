INSERT INTO tipo_utente (nome, descrizione) VALUES ('Amministratore', 'taglia le dita a tutti');
INSERT INTO tipo_utente (nome, descrizione) VALUES ('Cliente', 'colui che ha una fame feroce');
INSERT INTO tipo_utente (nome, descrizione) VALUES ('Fornitore', 'pregate che vi dia del cibo');
INSERT INTO tipo_utente (nome, descrizione) VALUES ('Fattorino', 'io consegno con una velocita super sonica');

INSERT INTO tipo_cucina(nome) VALUES ('Romagnola');
INSERT INTO tipo_cucina(nome) VALUES ('Street food');
INSERT INTO tipo_cucina(nome) VALUES ('Napolitana');

INSERT INTO categoria_ingrediente (nome) VALUES ('Vegetariano');
INSERT INTO categoria_ingrediente (nome) VALUES ('Vegano');
INSERT INTO categoria_ingrediente (nome) VALUES ('Ciliaco');

INSERT INTO ingrediente (nome, descrizione, id_categoria) VALUES ('Passata al pomodoro', 'Buonissima Passata al pomodoro', 1);
INSERT INTO ingrediente (nome, descrizione, id_categoria) VALUES ('Uovo di gallina', 'Buonissimo Uovo di gallina', 1);
INSERT INTO ingrediente (nome, descrizione, id_categoria) VALUES ('Latte', 'Buonissimo Latte', 1);

INSERT INTO citta(nome) VALUES ('Cesena');
INSERT INTO citta(nome) VALUES ('Forli');
INSERT INTO citta(nome) VALUES ('Bologna');

-- QUI gli id_fornitore non sono esatti
INSERT INTO listino (nome, attivo, id_fornitore) VALUES ('Menu del McDonalds', '1', 4);
INSERT INTO listino (nome, attivo, id_fornitore) VALUES ('Menu del ciccio', '1', 6);

INSERT INTO piatto (nome, descrizione, prezzo, peso, id_tipo_cucina) VALUES ('Verduriana1', 'Pizza completamente senza senso', 10, 1, 1);
INSERT INTO piatto (nome, descrizione, prezzo, peso, id_tipo_cucina) VALUES ('Verduriana2', 'Pizza completamente senza senso', 20, 1, 1);
INSERT INTO piatto (nome, descrizione, prezzo, peso, id_tipo_cucina) VALUES ('Verduriana3', 'Pizza completamente senza senso', 30, 1, 1);

INSERT INTO dettagli_piatto (id_piatto, id_ingrediente, quantita) VALUES (1, 1, 10);
INSERT INTO dettagli_piatto (id_piatto, id_ingrediente, quantita) VALUES (1, 2, 20);
INSERT INTO dettagli_piatto (id_piatto, id_ingrediente, quantita) VALUES (1, 3, 30);
INSERT INTO dettagli_piatto (id_piatto, id_ingrediente, quantita) VALUES (2, 1, 10);
INSERT INTO dettagli_piatto (id_piatto, id_ingrediente, quantita) VALUES (2, 2, 20);
INSERT INTO dettagli_piatto (id_piatto, id_ingrediente, quantita) VALUES (2, 3, 30);
INSERT INTO dettagli_piatto (id_piatto, id_ingrediente, quantita) VALUES (3, 1, 10);
INSERT INTO dettagli_piatto (id_piatto, id_ingrediente, quantita) VALUES (3, 2, 20);
INSERT INTO dettagli_piatto (id_piatto, id_ingrediente, quantita) VALUES (3, 3, 30);

INSERT INTO dettagli_listino (id_piatto, id_listino) VALUES (1, 1);
INSERT INTO dettagli_listino (id_piatto, id_listino) VALUES (2, 1);
INSERT INTO dettagli_listino (id_piatto, id_listino) VALUES (3, 1);

INSERT INTO tipo_pagamento(nome, tasso_transazione) VALUES ('Carta di credito', 0);
INSERT INTO tipo_pagamento(nome, tasso_transazione) VALUES ('Contanti alla consegna', 0);

INSERT INTO tipo_tariffa(nome) VALUES ('Tariffa ordinaria');
INSERT INTO tipo_tariffa(nome) VALUES ('Tariffa con supplemento');

INSERT INTO tipo_spedizione(nome, tariffa, id_tipo_tariffa) VALUES ('Ordinaria', 5, 1);
INSERT INTO tipo_spedizione(nome, tariffa, id_tipo_tariffa) VALUES ('Veloce', 6, 1);
INSERT INTO tipo_spedizione(nome, tariffa, id_tipo_tariffa) VALUES ('Urgente', 7, 1);

INSERT INTO stato_spedizione(nome, descrizione) VALUES ('In elaborazione', 'La consegna ancora non spedita');
INSERT INTO stato_spedizione(nome, descrizione) VALUES ('Spedito', 'La consegna spedita');
INSERT INTO stato_spedizione(nome, descrizione) VALUES ('In arrivo', 'La consegna in arrivo');
INSERT INTO stato_spedizione(nome, descrizione) VALUES ('Arrivata', 'La consegna arrivata');

INSERT INTO `luogoconsegna` (`id`, `nome`, `descrizione`) VALUES
(1, 'Ingresso piano terra', ''),
(2, 'Aula Magna 3.4', ''),
(3, 'Spazio Studenti', ''),
(4, 'Aula 2.1', ''),
(5, 'Lab VeLa 2.2', ''),
(6, 'Aula 2.3', ''),
(7, 'Aula 2.4', ''),
(8, 'Aula 2.5', ''),
(9, 'Aula 2.6', ''),
(10, 'Aula 2.7', ''),
(11, 'Aula 2.8', ''),
(12, 'Aula 2.9', ''),
(13, 'Aula 2.10', ''),
(14, 'Aula 2.11', ''),
(15, 'Aula 2.12', ''),
(16, 'Aula 2.13', ''),
(17, 'Laboratorio LaMo', ''),
(18, 'Laboratorio Disegno 2.17', ''),
(19, 'Laboratorio Disegno 2.16', ''),
(20, 'Laboratorio Disegno 2.15', ''),
(21, 'Laboratorio Disegno 2.14', ''),
(22, 'Laboratorio LIB', ''),
(23, 'Laboratorio LIB2', ''),
(24, 'Laboratorio informatico 3.1', ''),
(25, 'Laboratorio LeLe 3.2', ''),
(26, 'Laboratorio', 'Piano primo'),
(27, 'Laboratorio 1 Elettr e Telec', 'Piano primo'),
(28, 'Laboratorio ICM', 'Piano primo'),
(29, 'Laboratorio informatico 3.3', 'Piano primo'),
(30, 'Laboratorio disegno 3.5', 'Piano primo'),
(31, 'Laboratorio disegno 3.6', 'Piano primo'),
(32, 'Aula 3.7', 'Piano primo'),
(33, 'Laboratorio disegno 3.8', 'Piano primo'),
(34, 'Laboratorio disegno 3.9', 'Piano primo'),
(35, 'Laboratorio LaMo', 'Piano primo'),
(36, 'Laboratorio La Mo VidA', 'Piano primo'),
(37, 'Aula 3.10', 'Piano primo'),
(38, 'Aula 3.11', 'Piano primo'),
(39, 'Laboratorio di sisntesi 3.12', 'Piano primo'),
(40, 'Laboratorio di sisntesi 3.13', 'Piano primo'),
(41, 'Laboratorio di sisntesi 3.14', 'Piano primo'),
(42, 'Laboratorio di sintesi 3.15', 'Piano primo'),
(43, 'Laboratorio di sintesi 3.16', 'Piano primo'),
(44, 'Zona ristoro', 'Piano primo'),
(45, 'Ingresso piano primo', 'Piano primo');


INSERT INTO sconto(nome, valore) VALUES ('Sconto 1', 0);
INSERT INTO sconto(nome, valore) VALUES ('Sconto 2', 5);
INSERT INTO sconto(nome, valore) VALUES ('Sconto 3', 10);
INSERT INTO sconto(nome, valore) VALUES ('Sconto 4', 15);
INSERT INTO sconto(nome, valore) VALUES ('Sconto 5', 20);
INSERT INTO sconto(nome, valore) VALUES ('Sconto 6', 25);
INSERT INTO sconto(nome, valore) VALUES ('Sconto 7', 30);