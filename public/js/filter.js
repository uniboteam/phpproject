$(document).ready(function(){

  $(".filt").hide();

  var slider = document.getElementById("myRange");
  var output = document.getElementById("demo");
  output.innerHTML = slider.value;

  slider.oninput = function() {
    output.innerHTML = this.value;
  }

  $(".btn").click(function(){
    $(".filt").show();
    $(this).hide();
  });

  $(".btnFilt").click(function(){
    $(this).hide();
    $(".filt").hide();
    /**DOVRA INVIARE I DATI PER EFFETTUARE LA RICERCA CON IFILTRI APPLICATI**/
  });

});
