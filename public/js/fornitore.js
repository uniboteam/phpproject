function evadereOrdine(idOrdine) {
  $.ajax({
    url: "ordini/evadere/" + idOrdine,
    method: "POST",
    dataType: "json",
    data: null
  }).done(function(data) {
    if (data.status) {
      location.reload();
    }
  });
}
