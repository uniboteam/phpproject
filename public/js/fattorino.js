function cambiareStatoOrdine(idOrdine) {
  var idStato = $("#statoSpedizione_" + idOrdine).val();
  $.ajax({
    url: "ordine/" + idOrdine + "/changeStato/" + idStato,
    method: "POST",
    dataType: "json",
    data: null
  }).done(function(data) {
    if (data.status) {
      location.reload();
    }
  });
}
