$(document).ready(function(){
  $(".menu-subtitle").hide();

/*PER APPLICARE I FILTRI*/
  $("#menu-filters li a").click(function() {
    $("#menu-filters li a").removeClass('active');
    $(this).addClass('active');

    var selectedFilter = $(this).data("filter");
    //  $("#menu-wrapper").fadeTo(100, 0);

    //$(".menu-restaurant").fadeOut();

    setTimeout(function() {
      $(selectedFilter).slideDown();
      //$("#menu-wrapper").fadeTo(300, 1);
    }, 300);
  });

  $(".btn").click(function(){
    $(".menu-subtitle").toggle();
  });

});
