$(document).ready(function(){
 $(".add-to-cart").on('click', function(){
   var count=0;
   $("input[name=piatto_menu]").each(function(){
     var ischecked= $(this).is(":checked");
     if(ischecked){
       count++;
       $("a.add-to-cart").addClass("size");
       setTimeout(function() {
         $("a.add-to-cart").addClass("hover");
       }, 200);
       setTimeout(function() {
         $("a.cart > span").addClass("counter");
         $("a.cart > span.counter").text(count);
       }, 400);
       setTimeout(function() {
         $("a.add-to-cart").removeClass("hover");
         $("a.add-to-cart").removeClass("size");
       }, 600);
     }
     if(!ischecked){
        alert("Seleziona prima un piatto da aggiungere al carrello!");
     }
   });
 });
});
