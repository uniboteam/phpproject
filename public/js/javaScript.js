$(document).ready(function(){

  $("a.enter").click(function(){
      $(".main").prepend("<span class='tl'></span> <span class='tl'></span>");
      $(".main").append("<span class='bl'></span>'<span class='br'></span>");

    $(this).hide();
    $(".main").show();
    $(".page").addClass("loaded");

    $('.loaded .tl').delay(50).animate({top:'-100%',left:'-10%'},600);
    $('.loaded .tr').delay(600).animate({top:'-100%',left:'10%'},100);
    $('.loaded .bl').delay(400).animate({top:'100%',left:'10%'},100);
    $('.loaded .br').delay(200).animate({top:'100%',left:'-10%'},700);
  });

});
